image: node:18.16

stages:
  - yarn
  - deploy_review
  - test
  - perf
  - deploy_staging
  - deploy

cache: &yarn_cache
  paths:
    - node_modules
  policy: pull
  key:
    files:
      - yarn.lock

yarn:
  stage: yarn
  cache:
    <<: *yarn_cache
    policy: pull-push
  script:
    - yarn install
  artifacts:
    paths:
      - node_modules/
    expire_in: 30 min

lint:
  stage: test
  script:
    - 'yarn run lint-no-fix'

unit:
  stage: test
  script:
    - yarn run test:unit
  coverage: /All files[^|]*\|[^|]*\s+([\d\.]+)/
  artifacts:
    when: always
    paths:
      - coverage
      - junit.xml
    reports:
      junit: junit.xml
    expire_in: 30 days

e2e:
  image: mcr.microsoft.com/playwright:v1.35.0-jammy
  stage: test
  tags:
    - playwright
  script:
    - yarn run playwright test tests/e2e
  artifacts:
    when: always
    paths:
      - rspec.xml
    reports:
      junit: rspec.xml

perfs:
  image: mcr.microsoft.com/playwright:v1.35.0-jammy
  stage: perf
  allow_failure: true
  only:
    - dev
  tags:
    - playwright
  script:
    - yarn run playwright test tests/perfs
  artifacts:
    when: always
    paths:
      - rspec.xml
    reports:
      junit: rspec.xml

stress:
  image: mcr.microsoft.com/playwright:v1.35.0-jammy
  stage: test
  parallel: 7
  tags:
    - playwright
  script:
    - yarn run playwright test tests/stress/*.spec.ts --shard=$CI_NODE_INDEX/$CI_NODE_TOTAL
  rules:
    - if: $CI_PIPELINE_SOURCE == "push"
      when: manual
      allow_failure: true
  artifacts:
    when: always
    paths:
      - rspec.xml
      - stresstest-results
    reports:
      junit: rspec.xml
# Review Deployment

.setup-ssh-review: &setup-ssh-review
  before_script:
    # adapted from https://gitlab.com/gitlab-examples/ssh-private-key
    - "which ssh-agent || ( apt-get update -y && apt-get install openssh-client git -y )"
    - eval $(ssh-agent -s)
    - echo "$DEZRANN_DEV_SSH_B64" | base64 -d | ssh-add - > /dev/null
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - ssh-keyscan "$ALGOMUS_DEV_HOST" >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts

deploy_review:
  <<: *setup-ssh-review
  stage: deploy_review
  allow_failure: true
  # needs: ["lint", "unit", "e2e"]
  except:
    - master
  script:
    - npx browserslist@latest --update-db
    - yarn install
    - DEV_BUILD_PATH=$CI_COMMIT_REF_SLUG yarn run build --mode production
    - yarn run vue-cli-service build --target wc --no-clean --dest dist/wc --name dezrann-canvas src/wc/DezrannCanvas.vue
    - mv dist $CI_COMMIT_REF_SLUG
    - ssh dezrann-dev@$ALGOMUS_DEV_HOST "rm -rf /home/dezrann-dev/server/dev/$CI_COMMIT_REF_SLUG"
    - scp -r ./$CI_COMMIT_REF_SLUG dezrann-dev@$ALGOMUS_DEV_HOST:/home/dezrann-dev/server/dev/
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    url: http://$ALGOMUS_DEV_HOST/dev/$CI_COMMIT_REF_SLUG

# staging deployment

deploy_staging:
  <<: *setup-ssh-review
  stage: deploy_staging
  needs: ["lint", "unit", "e2e"]
  only:
    - dev
  script:
    - yarn install
    - yarn run build --mode staging
    - mv dist html
    - scp -r html dezrann-dev@$ALGOMUS_DEV_HOST:/var/www/test.dezrann.net.staging
    - |
      ssh dezrann-dev@$ALGOMUS_DEV_HOST "
      rm -rf /var/www/test.dezrann.net.bak/html
      mv /var/www/test.dezrann.net/html /var/www/test.dezrann.net.bak/
      mv /var/www/test.dezrann.net.staging/html /var/www/test.dezrann.net/
      "
  environment:
    name: staging
    url: http://$ALGOMUS_DEV_HOST


# App Deployment

.setup-ssh-production: &setup-ssh-production
  before_script:
    # adapted from https://gitlab.com/gitlab-examples/ssh-private-key
    - "which ssh-agent || ( apt-get update -y && apt-get install openssh-client git -y )"
    - eval $(ssh-agent -s)
    - echo "$DEZRANN_PROD_SSH_B64" | base64 -d | ssh-add - > /dev/null
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - ssh-keyscan "$ALGOMUS_HOST" >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts

deploy:
  <<: *setup-ssh-production
  stage: deploy
  needs: ["lint", "unit", "e2e"]
  only:
    - master
  script:
    - node -v
    - yarn install
    - yarn run build
    - mv dist html
    - scp -r html dezrann@$ALGOMUS_HOST:/var/www/dezrann.net.staging
    - |
      ssh dezrann@$ALGOMUS_HOST "
      rm -rf /var/www/dezrann.net.bak/html
      mv /var/www/dezrann.net/html /var/www/dezrann.net.bak/
      mv /var/www/dezrann.net.staging/html /var/www/dezrann.net/
      "

  environment:
    name: production
    url: http://$ALGOMUS_HOST

