
[Dezrann](https://www.dezrann.net) is an open platform to hear, study, and annotate music on the web.

This is the front/client part of Dezrann.
See the [home repository](https://gitlab.com/algomus.fr/dezrann/dezrann)
for more information on Dezrann.

# Documentation

See <https://doc.dezrann.net/> for the main Dezrann documentaion

- [Front Installation](docs/en/install.md)
- [Front Development Setup](docs/en/dev.md)

# Credits, reference

Dezrann is open-source (GPL v3+) and is developped by 
Emmanuel Leguy, Mathieu Giraud, Lou Garczynski, Charles Ballester, 
with the help of many [contributors](https://gitlab.com/algomus.fr/dezrann/dezrann/-/blob/dev/codemeta.json?ref_type=heads#L82) 
in the Algomus computer music team and elsewhere.