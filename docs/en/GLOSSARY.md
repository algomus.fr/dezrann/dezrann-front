# Glossary

* **App**: the entire front-end single page application
* **Actions**: an API provided to behaviors and edit bars, allowing them to modify the model and a few other states, such as selection
* **Analysis**: the model of the AnalysisEditor page, and the parsed result of a .dez file. Contains all information that may be displayed by the LabelCanvas, and saved into a file
* **Behavior**: see [Behaviors](./BEHAVIORS.md)
* **Decorator**: A call to a function able to modify a following declaration/initialisation. They cannot modify a typescript type, but can add functionality and behaviors to any standard js declaration. They are used by `vue-property-decorator` to inline the descriptions of a component.
* **Label**: an annotation added to a particular moment of a music. It can refer to the text format labels inside a .dez file, to the `Label` interface representing that information, or to the LabelSvg component
* **LabelCanvas/Canvas**: a vue component able to display a list of labels, update them with the model, and collect all DOM events from its internal elements. It can contain behaviors.
* **Page/View**: A special case of the Vue component, as it is not fully a child of another component, but created by `vue-router`, and inserted into the App's `router-view`s
* **Portal**: a component from the `portal-vue` library, that allows us to display the logical child of a component A inside a component B. This is extremely useful, as any page can insert or override parts of the sidebar, which otherwise has sensible default values.
* **Props**: data passed from a parent component to its children
* **Scale**: the non-uniform  horizontal scaling applied to spectrograms and waveform canvases, requires a redraw for each label
* **Sidebar**: the component on the left of the dezrann app. Can be one of two different components, and is also loaded dynamically by the router
* **Sync/Synchronization**: the unit converting musical time to an audiofile timestamp, can be modified by `SyncEditor`
* **Unit**: an object representing the conversion between musical time and a particular unit, such as pixel position or an audio performance timestamp
* **Zoom**: the uniform scaling applied to canvases. It is simply a DOM transform, and can be applied without redraws
