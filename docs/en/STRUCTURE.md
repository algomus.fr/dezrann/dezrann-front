# Structure

## Introduction

Dezrann is made to be robust yet quick to prototype.

For that, only two principles are held:

* dependencies should only go one way

* no abstractions until needed

The dependency ladder looks like this:

* The App ([1](../../src/views/App.vue), [2](../../src/views/PiecePage.vue), [3](../../src/components/AnalysisEditor.vue)) uses the canvas and behaviors with the client into a useful application

* [Behaviors](../../src/components/behaviors) implement useful user interactions using events and an abstract API provided to them

* The [Canvas](../../src/components/canvas/LabelCanvas.vue) displays an Analysis, and provides events

* The [Client](../../src/client/Client.ts) loads and converts the Dezrann format into an Analysis

* The [Analysis](../../src/data/Analysis.ts) provides a reactive data structure for all labels on the page and an API to modify them.

* The [format](../../src/formats/analysis-formats.ts) objects define in typescript and AJV schemas what a valid .dez file look like

These elements can be aware of elements under them in the ladder, but are always agnostic of elements above. For example, while the App can load any behaviors with minimal modification, it still needs to know what to provide them, and how to integrate their events into external elements. On the other hand, behaviors have no business with the Client.

Here's the dependency graph of the entire App

<img src="../graphs/archi.svg">

Here's the per-file dependency graph of the Client, to show the single direction of dependencies

<img src="../graphs/client.svg">

## Behaviors

Behaviors are a design pattern used to implement any kind of user interaction with the app, especially the canvas.

For example, the behavior `BSelectOnClick` listens to DOM `'click'` click events, and outputs an `'input'` event, which is used by Vue for upwards data transfer.

More info on [Behaviors](./BEHAVIORS.md)

<img src="../graphs/behaviors.svg">

## Components

Vue components are allowed to depend on all typescript file, which can make for complicated dependency graphs. For example, the upload button must depend on both the Client and the routing utilities to work correctly, while the input components may depend on parsing utilities.

Here is a graph showing the general structure of the components, as well as the integration role of the [AnalysisEditor](../../src/components/AnalysisEditor.vue).

<img src="../graphs/components.svg">

## Synchronization

Dezrann is packaged with another smaller app, the synchronization editor.

It follows a very similar structure to the main app, but has its own behavior, APIs, and editor bars on top.

<img src="../graphs/components-sync.svg">

## Sidebar

The sidebar uses `portal-vue` extensively, to inject page specific content. This has overall proven to be a good, flexible solution.

All page-specific info and buttons should be declared in the View, or one of its children.

<img src="../graphs/components-sidebar.svg">

## External dependencies policy

Dezrann-front will be split in the future into a format library, a client and the app.

No mandatory external dependency in the format library will be allowed, and only minimal dependencies will be allowed in the client.

Dependencies in the app should be typed, safe, well-used and not abandonned.

## Warnings

The App is also heavily dependant on reactivity. Any objects not created inside a Vue component cannot be reactive, and therefore will not trigger a DOM update on change.

Watchers are used extensively, which is not always best practice. They often mutate state, which is too be avoided.

Prefer events and computed properties if possible, but do remember that events are untyped, and computed properties are cached.

The Client's error handling is still only a skeleton, and very different errors may receive the same code.

## Existing Anti-Patterns

Vue encourages using components that never modify objects passed to them, but use a functional model, that returns new objects to replace the old one. This model however does not work well with our application, since the analysis object can be huge, and may be modified by multiple behaviors at the same time. That is why we use an API instead, that can be easily mocked, to modify the analysis from components.

Multiple objects, like `AnalysisActions` that should be Vue agnostic use 'counters'. These counters are updated after some modifications, to allow the App to 'Watch' those events using reactivity, like a listener. This is very useful, but should only be used if no other options exist, as they are untyped and inelegant.

Some elements that should be behaviors are not. For example, the selection arrows could perfectly well be a behavior, instead of being hard-coded into the Canvas.

The folder structure does not always represent sensible grouping, and is subject to change.
