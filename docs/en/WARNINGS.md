# Warnings

## Reactivity

Vue does not always react to data change as you might expect.

Most of its magic comes from proxies, objects that wrap around normal javascript objects, and catch any modifications.

This means that you cannot expect reactivity from objects declared outside of Vue. Unless vue has had a chance to sneak a proxy around it, you cannot watch such objects.

Any component variables initialized to undefined, or not initialized, will not be reactive. Vue does not know they exist.

Vue also caches `computed properties`. This can be a very strange thing to debug, as you would expect your `get` functions to be called at each redraw.

## Dependencies

Upgrades should be done using `yarn interactive-upgrade [--latest]`

Upgrading dependencies can be harrowing. What you should know here is that:

* The vue-cli cache loader warning is not critical, and we are waiting for a fix from vue-cli
* Same for the "onBeforeSetupMiddleware is deprecated" warning
* Any new vue dependencies should be checked: is it vue 3 only? Are all its latest versions compatible with vue 2?
* vue-i18n-loader is an old version for a reason, newer versions DO NOT WORK for us
* vue-jest is a hot mess at any times, upgrade carefully
* sass upgrades can reasonably be ignored for now, they cause a lot of issues and we do not use newer sass features

## Dynamic Loading

Sidebars and Pages are not loaded at the same time as App.vue. They are instead dynamically requested and inserted into the Application.

Because of this, `App.vue`, `main.ts` and other mandatory imports should have as little runtime dependencies as possible, because they are the chokepoint of the tree-shaking algorithm. Anything they import must be sent to the user, whatever page they are browsing.

## Readonly Data

Inside a component, Props and values passed through slots should never be reassigned. Their own property values may be modified, but it is bad practice.

## Type Checking

While the whole project is in typescript, many elements are not type-checked because of vue.

This is why it is vital to use the Vetur extensions, which adds a massive amount of type checks to vue templates.

## Performances

By far the largest cost in a vue App are needless redraws.

You can limit redraws by:

* compartmentalizing each components as much as possible
* avoiding internal state
* using `key` wisely (this can both improve and decrease performance)

You can measure redraws using the Vue browser extension
