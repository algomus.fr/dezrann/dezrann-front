

# Dezrann-front development

See [Structure](docs/en/STRUCTURE.md) for the philosophy behind the development of Dezrann-front.

### UI Project Management

```sh
yarn global add @vue/cli
# or npm i -g @vue/cli

vue ui
```

### Setup

```sh
yarn
```

### Compiles and hot-reloads for development

```sh
yarn run serve
```

### Compiles and minifies for production

```sh
yarn run build
```

### Run production build

```sh
cd dist
yarn global add serve
# or npm i -g serve
serve -s .
```

### Run unit tests

```sh
yarn run test:unit
```

### Lint and fix files

```sh
yarn run lint
```

### Validate analysis files (.dez)

```sh
yarn run test:unit ./tests/unit/files/check-analyses.spec.ts
```
This validates the `.dez` files stored in `tests/unit/files/analyses/`

Other analyses can be added:

```sh
ln -sr YOUR_ANALYSES_FOLDER ./tests/unit/files/analyses/LINK_NAME
```

### Setup development environment

* Install Vue DevTools on firefox or chrome to debug components
* Install recommended vscode extensions
  * ESLint
    * Display linting errors and fixes them
  * Vetur
    * Syntax highlighting and autocompletion for Vue
  * Code Spell Checker
    * Displays spelling mistakes
  * Path Intellisense
    * Adds better path auto-completion when importing from vue files
  * Multi Cursor Case Preserve
    * Allows vue snippets to dictate the case of the template arguments
  * Vscode Markdownlint
    * Syntax highlighting for Markdown files
  * Debugger for Firefox
    * Allows attaching to the firefox debugging API
  * Debugger For Chrome
    * Allows attaching to the chrome debugging API
    * Not required for modern versions of VS Code
* Get YAML syntax highlighting for i18n blocks
  * Go to a file with a `<i18n>` block
  * Run the command `Vetur: Generate Grammar for...`
  * Run the command `Developper: Reload Window`

### Update Project

See [Warnings](./docs/en/WARNINGS.md)

### Debugging

* Check that the chrome or firefox debuggers are installed
* Select a configuration from the vscode debug dropdown
* Run configuration

#### On Chrome

* open DevTools (ctrl+shift+i)
* in sources tab, go to `dezrann-front/./src`
* add a break point in the file of your choice
* use dezrann-front to debug

