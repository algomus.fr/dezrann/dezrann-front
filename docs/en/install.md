

# Dezrann-front local usage

## Local installation

Install `Node.js` (18), `npm`, `yarn`, and their dependencies.

### On Debian/Ubuntu systems

```sh
apt-get install nodejs npm
npm install --global yarn
```

You can also use nvm if you have several projects that need different versions of node:

```sh
curl -fsSL https://raw.githubusercontent.com/nvm-sh/nvm/vX.X.X/install.sh | bash
source ~/.bashrc # for bash
nvm install node # for latest version
```


### On Windows

Download and install Node.js from <https://nodejs.org>
Ensure you are using Node.js 18 with the command

    node -v 

If the version is 20 or above you will need to use nvm to change to 18.  
You can download nvm from <https://github.com/coreybutler/nvm-windows/releases> then use 

    nvm install 18
    nvm use 18
  

### Run a local dezrann-front client


```sh
yarn
yarn run serve
```

Dezrann-front will then be available at <http://localhost:8080/> or a similar address.

You can then access both
- **"local" pieces,** hosted by dezrann-front (see [Local corpus](docs/en/local-corpus))
- **"remote" pieces,** hosted by a dezrann backend (that may include [public built corpora](https://doc.dezrann.net/rebuild))

### Use of Dezrann backend

#### dezrann.net services

By default the app is configured to use <ws.dezrann.net> as a backend. You can access to public corpus without authentication. Il you have an account on <dezrann.net>, you can access to other corpus if you have enough permissions.

#### Running and using a local backend

Follow the instructions in:

- [Dezrann-back README](https://gitlab.com/algomus.fr/dezrann/dezrann-back/-/blob/dev/)
- [Dezrann authentication server README](https://gitlab.com/algomus.fr/dezrann/dezrann/-/tree/dev/code/dez-server/dez-auth/)

Then, on the frontend, update `.env.developpement` file with local FQDNs:

```
VUE_APP_AUTH_URL=http://localhost:9999/authenticate/
VUE_APP_CHPWD_URL=http://localhost:9999/changepw/
VUE_APP_CORPUS_URL=http://localhost:8000/
...
```

## Install for production

### Config

Put your backend FQDNS in `.env.production`.

### Build the app

```
yarn install
yarn run build
```

Copy the build dir into your http server:
```
scp -r dist/* ${admin-user}@${server-fqdn}:/path/to/document/root/
```
