import type { Pixel } from '@/data';

/**
 * minimum onsets coverate rate in the synchronization to use these points to snap
 */
export const MIN_ONSETS_COVERAGE_RATE = 0.8;

const SNAP_PIXELS_PRECISION = 20;

export function roundX(x: number): Pixel {
  return Math.round(x / SNAP_PIXELS_PRECISION) * SNAP_PIXELS_PRECISION as Pixel;
}
