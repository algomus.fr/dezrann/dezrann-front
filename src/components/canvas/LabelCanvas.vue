<template>
  <svg
    ref="canvas"
    class="canvas"
    :class="{ 'canvas-margins': !source.shouldSnap}"
    :height="canvasHeight * zoom"
    :width="canvasWidth * zoom"
    :view-box="`0 0 ${canvasHeight} ${canvasWidth}`"
    v-on="getListeners()"
  >
    <!-- The canvas scaler -->
    <g
      transform-origin="top left"
      :transform="`scale(${zoom}, ${zoom})`"
    >
      <g>
        <!-- The background color -->
        <rect
          fill="white"
          :width="canvasWidth"
          :height="canvasHeight"
        />
        <!-- The background image -->
        <g v-if="showImage" :transform="renderStaves ? '' : `translate(0, -${getLinesHeight})`">
          <slot name="svgInjector" />
        </g>

        <!-- The staff rects, used for contextual events -->
        <!-- They produce 'staff-' events on input -->
        <!-- The 'all' staff never receives events, because it cannot be dragged to or from -->
        <g v-if="['3d', 'grid'].includes(source.type)">
          <g v-for="(page, pIndex) in source.pages" :key="pIndex">
            <g v-for="(row, rIndex) in page.rows" :key="rIndex">
              <rect
                v-for="(staff, index) in row.staves"
                :key="index"
                x="0"
                :width="canvasWidth"
                :y="(page.height * pIndex) + staff.top + PADDING_SHADE / 2"
                :height="staff.height - PADDING_SHADE"
                class="staff"
                :class="{
                  'click-through': staff.name === 'all',
                  'show-line': showLines && isLineStaff(staff)
                }"
                data-canvas-tag="staff"
                :data-canvas-id="staff.id"
              />
            </g>
          </g>
        </g>
        <g v-else-if="renderStaves">
          <rect
            v-for="(staff, index) in source.staves"
            :key="index"
            x="0"
            :width="canvasWidth"
            :y="staff.top + PADDING_SHADE / 2"
            :height="staff.height - PADDING_SHADE"
            class="staff"
            :class="{
              'click-through': staff.name === 'all',
              'show-line': showLines && isLineStaff(staff)
            }"
            data-canvas-tag="staff"
            :data-canvas-id="index"
          />
        </g>
        <g v-else>
          <rect
            v-if="source.staves.find((s) => s.name === 'all')"
            x="0"
            :width="canvasWidth"
            :y="source.staves.find((s) => s.name === 'all').top + PADDING_SHADE / 2"
            :height="computeNoStavesRenderHeight()"
            class="staff"
            data-canvas-tag="staff"
            :data-canvas-id="source.staves.find((s) => s.name === 'all').id"
          />
        </g>
      </g>
      <!-- The behavior slots -->
      <slot :canvas="canvas" />
      <!-- The label container -->
      <label-group-svg
        v-if="sortedLabels"
        :selected-id="selectedId"
        :has-margin="source.shouldSnap"
        :sorted-labels="sortedLabels"
        :pixel-unit="source.pixelUnit"
        :staves="source.staves"
        :is-focused="isFocused"
        :current-view-x="currentViewX"
        :pages="source.pages"
        :current-page="currentPage"
        :source-type="source.type"
      />
    </g>
  </svg>
</template>
<script lang="ts">
import {
  Component, Prop, Ref, Vue,
} from 'vue-property-decorator';
import type {
  Label, LabelId, StaffId, Pixel, Source, Staff,
} from '@/data';
import { StaffType } from '@/data';
import PixelUtils from '@/data/PixelUtils';
import LabelGroupSvg from './LabelGroupSvg.vue';
import type {
  BaseEventName,
  CanvasMouseEvent,
  EventName,
  SourceTag,
  Registerer,
  CanvasEventSource,
} from './canvas-events';
import {
  canvasEventIsModifiable,
  canvasSourceIsLabel,
} from './canvas-events';

const LINES_HEIGHT = 102;

@Component({ components: { LabelGroupSvg } })
export default class LabelCanvas extends Vue {
  PADDING_SHADE = 4;

  get canvasHeight() {
    if (!this.renderStaves) {
      return this.source.height;
    }
    if (this.source.pages) {
      return (this.source.pages.length * this.source.height) + (LINES_HEIGHT * 2);
    }
    return this.source.height + LINES_HEIGHT * 2;
  }

  get canvasWidth() {
    return this.source.width;
  }

  get getLinesHeight(): number {
    return LINES_HEIGHT;
  }

  @Prop({ default: null })
  selectedId!: LabelId | null;

  @Prop({ required: false, default: true, type: Boolean })
  renderStaves!: boolean;

  /**
   * If the canvas is allowed to ask for DOM focus
   */
  @Prop({ required: true })
  isFocused!: boolean;

  @Prop({ required: false })
  getLabel?: (labelId: LabelId) => Readonly<Label> | null;

  @Prop({ required: true })
  sortedLabels!: Readonly<Label>[];

  @Prop({ required: true })
  source!: Source;

  @Prop({ default: 1 })
  zoom!: number;

  @Prop({
    default: 0,
    type: Number,
  })
  currentViewX!: Pixel;

  @Ref('canvas')
  svg!: SVGSVGElement;

  @Prop({ default: false })
  showLines!: boolean;

  @Prop({ required: false, type: Boolean, default: true })
  showImage!: boolean;

  currentPage = 0;

  /**
   * A function allowing anyone to bind a listener to the canvas
   * Passed to the behavior slot, to allow behaviors to listen to the canvas
   */

  get canvas(): CanvasEventSource {
    return {
      on: this.$on.bind(this) as Registerer,
      off: this.$off.bind(this),
      emit: this.$emit.bind(this),
    };
  }

  canEmit(): boolean {
    // If an SVG is hidden, its clientHeight is 0
    // If the svg is hidden, getScreenCTM will return null, and event handling will fail
    return !!this.svg?.clientHeight;
  }

  isLineStaff(staff: Staff): boolean {
    return staff.type === StaffType.line;
  }

  toCanvasEvent(event: MouseEvent): CanvasMouseEvent {
    // https://stackoverflow.com/questions/10298658/mouse-position-inside-autoscaled-svg

    // Create an SVGPoint at mouse position
    const pt = this.svg.createSVGPoint();
    pt.x = event.clientX;
    pt.y = event.clientY;

    const screenMatrix = this.svg.getScreenCTM();

    if (!screenMatrix) throw new Error('Could not convert mouse position');

    const { x, y } = pt.matrixTransform(screenMatrix.inverse());

    return {
      inner: event,
      x: (x / this.zoom) as Pixel,
      y: (y / this.zoom) as Pixel,
    };
  }

  /**
   * Create a v-on object
   * This object is a list of events to listen to, and their listener
   * The listener re-emits the event from the canvas with a new name
   * The listener also adds a relevant payload to each event
   */
  getListeners(): Record<`!${BaseEventName}`, unknown> {
    return {
      '!click': this.onCanvasEvent,
      '!dblclick': this.onCanvasEvent,
      '!mouseup': this.onCanvasEvent,
      '!mousedown': this.onCanvasEvent,
      '!mousemove': this.onCanvasEvent,
      '!mouseover': this.onCanvasEvent,
      '!wheel': this.onCanvasEvent,
      '!keydown': this.onCanvasEvent,
      '!keyup': this.onCanvasEvent,
      '!focusin': this.onCanvasEvent,
    };
  }

  // Get the '[ctrl-][shift-]event' format from an event
  getModifiers(e: MouseEvent | KeyboardEvent | FocusEvent): EventName {
    const name = e.type as BaseEventName;
    if (!('ctrlKey' in e)) return name;

    if (!canvasEventIsModifiable(name)) return name;

    if (e.ctrlKey && e.shiftKey && e.altKey) return `ctrl-shift-alt-${name}` as const;
    if (e.ctrlKey && e.shiftKey) return `ctrl-shift-${name}` as const;
    if (e.shiftKey && e.altKey) return `shift-alt-${name}` as const;
    if (e.ctrlKey && e.altKey) return `ctrl-alt-${name}` as const;
    if (e.shiftKey) return `shift-${name}` as const;
    if (e.ctrlKey) return `ctrl-${name}` as const;
    if (e.altKey) return `alt-${name}` as const;
    return name;
  }

  // Transform DOM events to canvas events
  onCanvasEvent(e: MouseEvent | KeyboardEvent| FocusEvent): void {
    if (!this.canEmit()) return;

    const target = e.target as SVGElement;
    const tag = target.dataset.canvasTag as SourceTag | undefined;
    const id = target.dataset.canvasId as string | undefined;

    // If the event is not from the correct elements, return
    // If the canvas is loading, return
    if (tag === undefined || !this.svg) return;

    // The payload to send to the behaviors
    const payload: unknown[] = [];

    // Add the event to the payload
    // If it's a mouse event, wrap it with more info
    if (e instanceof MouseEvent) payload.push(this.toCanvasEvent(e as MouseEvent));
    else payload.push(e);

    // The staffId if it is recovered later
    let staffId: StaffId | null = null;

    // Get and push the label in the payload
    if (canvasSourceIsLabel(tag)) {
      // Ignore label events if getLabel is not passed
      if (!this.getLabel) return;
      const labelId: LabelId = tag === 'label'
        ? parseInt(id as string, 10)
        : this.selectedId as LabelId;
      const label = this.getLabel(labelId);
      if (!label) {
        // eslint-disable-next-line no-console
        console.log(`Label ${labelId} is gone, ignoring event`);
        return;
      }

      payload.push(label);
      // Add the staff id so the staff can be recovered
      staffId = label.staffId;
    } else if (tag === 'staff') {
      // Parse the staff id directly
      staffId = parseInt(id as string, 10);
    }

    // Recover the staff
    if (staffId !== null) {
      let staff = this.source.staves[staffId];
      if (!staff) {
        staff = PixelUtils
          .getStaffByPosition(this.source.pages || [], staffId) || this.source.staves[0];
        // [staff] = [...this.source.staves];
        // throw new Error('Could not find staff');
      }
      payload.push(staff);
    }

    // Transform events from things like 'mousedown' to 'shift-mousedown'
    const fullName = this.getModifiers(e);

    // Emit to the specific listeners and the global listeners
    this.$emit(`${tag}-${fullName}`, ...payload);
    this.$emit(`all-${fullName}`, ...payload);
  }

  // Document event reemiting system

  reemit(e: MouseEvent | KeyboardEvent | FocusEvent) {
    // Skipping events if canvas is not loaded
    if (!this.canEmit()) return;

    const packet = e instanceof MouseEvent
      ? this.toCanvasEvent(e)
      : e;

    if (e.type === 'keydown' || e.type === 'keyup') {
      const tag = (e.target as HTMLElement).tagName;
      if (tag === 'INPUT' || tag === 'SELECT' || tag === 'BUTTON' || tag === 'TEXTAREA') {
        // Ignore keyboard events on input elements
        return;
      }
    }
    const name = this.getModifiers(e);
    this.$emit(`document-${name}`, packet);
  }

  computeNoStavesRenderHeight(): number {
    const allStaffHeight = this.source.staves.find((s) => s.name === 'all')?.height || 0;
    const additionalStaffHeight = this.source.staves.find((s) => s.name === 'top.1')?.height || 0;
    return allStaffHeight - this.PADDING_SHADE - additionalStaffHeight * 6;
  }

  mounted() {
    const capture = true;
    const passive = false;
    document.addEventListener('click', this.reemit, { capture, passive });
    document.addEventListener('wheel', this.reemit, { capture, passive });
    document.addEventListener('dblclick', this.reemit, { capture, passive });
    document.addEventListener('mouseup', this.reemit, { capture, passive });
    document.addEventListener('mousedown', this.reemit, { capture, passive });
    document.addEventListener('mousemove', this.reemit, { capture, passive });
    document.addEventListener('keydown', this.reemit, { capture, passive });
    document.addEventListener('keyup', this.reemit, { capture, passive });
    document.addEventListener('focusin', this.reemit, { capture, passive });
  }

  unmounted() {
    const capture = true;
    document.removeEventListener('click', this.reemit, { capture });
    document.removeEventListener('wheel', this.reemit, { capture });
    document.removeEventListener('dblclick', this.reemit, { capture });
    document.removeEventListener('mouseup', this.reemit, { capture });
    document.removeEventListener('mousedown', this.reemit, { capture });
    document.removeEventListener('mousemove', this.reemit, { capture });
    document.removeEventListener('keydown', this.reemit, { capture });
    document.removeEventListener('keyup', this.reemit, { capture });
    document.removeEventListener('focusin', this.reemit, { capture });
  }
}

</script>
<style scoped lang="scss">
// Allow bar staff to ignore clicks
.click-through {
  pointer-events: none;
}
// Remove focus outline in favor of custom one
:focus{
  outline: none;
}
// Styles the staves
.staff{
  fill: transparent;
  border: 1px solid black;
  transition: fill linear 0.2s;
  &.show-line {
    fill: #0000000a;
    transition: fill linear 0.2s;
  }
}
// Adds margins to the sides of waveforms
.canvas-margins {
  margin-left: 40px;
  margin-right: 40px;
  overflow: visible;
}
</style>
