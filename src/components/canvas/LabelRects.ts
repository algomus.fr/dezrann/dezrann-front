import type {
  Duration, Label, Onset, Pixel, Rect, Staff, Unit,
} from '@/data';
import { PixelUnitWithSeconds, SourceType } from '@/data';
import type { DeMuxPixel } from '@/data/PixelUtils';
import PixelUtils from '@/data/PixelUtils';
import type { Image3DPageData, Image3DRowData } from '@/data/Source';

function staffFromId(staffId: number, row: Image3DRowData) {
  return row.staves[staffId]
    || row.staves.find((s) => s.id === staffId)
    || row.staves[0];
}

function staffVerticalData(staff: Staff, yOrigin: number) {
  return [
    (Math.round(staff.top) + 1) + yOrigin as Pixel,
    Math.round(staff.height) - 2 as Pixel,
  ];
}

function rectHorizontalData(
  start: DeMuxPixel,
  end: DeMuxPixel,
  row: Image3DRowData,
  rowIndex: number,
) {
  if (rowIndex === start.row) { // Is Start row
    return [
      start.x,
      ((rowIndex === end.row) ? end.x : row.end) - start.x,
    ];
  }
  return [
    row.start,
    ((rowIndex > start.row && rowIndex < end.row) ? row.end : end.x) - row.start,
  ];
}

export class LabelRects {
  pixelUnit: Unit<Pixel>;

  label: Label;

  sourceType: SourceType;

  pages: Image3DPageData[];

  hasMargin: boolean;

  constructor(
    pixelUnit: Unit<Pixel>,
    label: Label,
    sourceType: SourceType,
    pages: Image3DPageData[],
    hasMargin: boolean,
  ) {
    this.pixelUnit = pixelUnit;
    this.label = label;
    this.sourceType = sourceType;
    this.pages = pages;
    this.hasMargin = hasMargin;
  }

  get value(): Rect[] {
    const rectsToRender: Rect[] = [] as Rect[];
    const [start, end] = this.labelRange();

    for (let pageIndex = start.page; pageIndex <= end.page; pageIndex += 1) {
      const [s, e] = this.label3DRange(start, end, pageIndex);
      rectsToRender.push(...this.computeRowRects(s, e, pageIndex));
    }
    return rectsToRender as Rect[];
  }

  private computeRowRects(
    start: DeMuxPixel,
    end: DeMuxPixel,
    pageIndex: number,
  ): Rect[] {
    const currentPage = this.pages[pageIndex];
    const yOrigin = pageIndex * currentPage.height;

    return this.labelRects(start, end, yOrigin, currentPage);
  }

  private labelRects(
    start: DeMuxPixel,
    end: DeMuxPixel,
    yOrigin: number,
    page: Image3DPageData,
  ) {
    const rectsToRender: Rect[] = [] as Rect[];
    for (let i = start.row; i <= end.row; i += 1) {
      rectsToRender.push(this.rect(start, end, yOrigin, page.rows[i], i));
    }
    return rectsToRender;
  }

  private rect(
    start: DeMuxPixel,
    end: DeMuxPixel,
    yOrigin: number,
    row: Image3DRowData,
    rowIndex: number,
  ) {
    const [x, width] = rectHorizontalData(start, end, row, rowIndex) as [Pixel, Pixel];
    if (this.isFullHeightInGrid) {
      return {
        x,
        y: row.y as Pixel,
        width,
        height: row.height as Pixel,
      };
    }
    const staff = staffFromId(this.label.staffId, row);
    const [staffY, staffHeight] = staffVerticalData(staff, yOrigin);
    return {
      x: this.margifiedX(x),
      y: staffY as Pixel,
      width: this.margifiedWidth(width),
      height: staffHeight as Pixel,
    };
  }

  private get shouldAddMargin(): boolean {
    return this.hasMargin || !this.label.duration;
  }

  private get shouldAddNegativeMarginRight(): boolean {
    return this.label.duration && this.label.durationType === 'duration';
  }

  private get margin() {
    return this.hasMargin ? 8 : 2;
  }

  private margifiedX(x: number): Pixel {
    return (this.shouldAddMargin ? x - this.margin : x) as Pixel;
  }

  private margifiedWidth(width: number): Pixel {
    return (
      this.shouldAddMargin && !this.shouldAddNegativeMarginRight
        ? width + this.margin * 2
        : width
    ) as Pixel;
  }

  private get isFullHeightInGrid(): boolean {
    return this.sourceType === SourceType.Grid
    && ['Harmony', 'Gesture', 'Pattern'].includes(this.label.type);
  }

  private labelRange() {
    let range;
    if (PixelUnitWithSeconds.isUnitWithSeconds(this.pixelUnit)) {
      range = this.pixelUnit.rangeFromInterval(
        this.label.onset,
        this.label.duration,
        this.label.repeat,
      );
    } else if (this.sourceType === SourceType.Grid) {
      range = this.pixelUnit.fromInterval(
        this.label.onset + 0.001 as Onset,
        this.label.duration - 0.001 as Duration,
      );
    } else {
      range = this.pixelUnit.fromInterval(
        this.label.onset,
        this.label.duration,
      );
    }
    return [
      PixelUtils.deMux(range.start),
      PixelUtils.deMux(range.end),
    ];
  }

  private label3DRange(
    start: DeMuxPixel,
    end: DeMuxPixel,
    pageIndex: number,
  ): [DeMuxPixel, DeMuxPixel] {
    const lastRow = this.pages[pageIndex].rows.length - 1;
    const lastX = this.pages[pageIndex].rows[lastRow].end;
    const firstX = this.pages[pageIndex].rows[0].start;
    if (pageIndex === start.page) {
      return [
        start,
        (pageIndex === end.page) ? end : { page: start.page, row: lastRow, x: lastX },
      ];
    }
    return [
      { page: pageIndex, row: 0, x: firstX },
      (pageIndex > start.page && pageIndex < end.page)
        ? { page: pageIndex, row: lastRow, x: lastX }
        : end,
    ];
  }
}
