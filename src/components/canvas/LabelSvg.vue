<template>
  <g
    :class="{
      'disabled': isLabelDisabled,
      'grid-disabled-background': isGridBackground,
      'grid-disabled-harmony': isGridHarmony,
      'grid-disabled-structure': isGridStructure,
      'grid-gesture': isGridGesture
    }"
  >
    <circle
      v-if="isMomentBar"
      :cx="displayRects[0].x + 2"
      :cy="displayRects[0].y - 10"
      r="10"
      class="label-rect"
      :class="classes"
      data-canvas-tag="label"
      :data-canvas-id="label.id"
    />
    <g ref="rect">
      <rect
        v-for="(displayRect, index) in displayRects"
        :key="index"
        :x="displayRect.x"
        :y="displayRect.y"
        :width="displayRect.width"
        :height="displayRect.height"
        tabindex="0"
        class="label-rect"
        :class="classes"
        data-canvas-tag="label"
        :data-canvas-id="label.id"
      />
      <image
        v-if="label.icon"
        :xlink:href="label.icon"
        :x="displayRects[0].x + 5"
        :y="displayRects[0].y + 5"
        :width="displayRects[0].height - 10"
        :height="displayRects[0].height - 10"
        class="label-icon"
      />
    </g>
    <g>
      <text
        v-if="!label.icon"
        ref="text"
        :x="isGridStructure ? 50 : textPos.x"
        :y="textPos.y"
        class="label-text"
        :class="classes"
      />
    </g>
    <selection-arrows
      v-if="isSelected && label.isResizable"
      :label="label"
      :staves="staves"
      :pixel-unit="pixelUnit"
      :has-margin="hasMargin"
      :pages="pages"
      :current-page="currentPage"
    />
  </g>
</template>
<script lang="ts">
import {
  Component, Prop, Ref, Watch,
} from 'vue-property-decorator';
import {
  SourceType, type Pixel, type Point, type Rect,
} from '@/data';
import LabelWatcher from './LabelWatcher.vue';
import SelectionArrows from './SelectionArrows.vue';

const MAX_BAR_TAG_WIDTH = 150;
const TEXT_MARGIN = 4;
const ELLIPSIS_WIDTH = 15;
const LABEL_TEXT_HEIGHT = 25;
const MOMENT_BAR_CIRCLE_DIAMETER = 10;
const TEXT_RIGHT_MARGIN = 16;

@Component({ components: { SelectionArrows } })
export default class LabelSvg extends LabelWatcher {
  @Prop({ default: false })
  isFocused!: boolean;

  @Prop({
    default: 0,
    type: Number,
  })
  currentViewX!: Pixel;

  /**
   * A reference to the label <rect> element
   */
  @Ref('rect')
  readonly svgRect!: SVGRectElement;

  /**
   * A reference to the <text> element
   */
  @Ref()
  readonly text!: SVGTextContentElement;

  @Watch('label.tag')
  @Watch('label.type')
  updateLabelText() {
    this.renderCount += 1;
  }

  /**
   * Updates the text value through the DOM
   * This is necessary as SVG does not support CSS text-overflow
   * We have to read the rastered text width through the DOM
   */
  @Watch('rects')
  updateText() {
    const tag = this.label.symbol || this.label.tag || '';

    // Calculate the max text width
    // If the label is a bar, use a static constant
    const width = this.label.duration
      ? this.rects[0].width - TEXT_MARGIN * 2
      : MAX_BAR_TAG_WIDTH;

    this.text.textContent = tag;

    // If the text fits, finish the function
    if (this.text.getComputedTextLength() < width) return;

    const availableWidth = width - ELLIPSIS_WIDTH;

    // If the text does not fit, iterate the length until it does
    for (let i = tag.length - 1; i >= 0; i -= 1) {
      const textWidth = this.text.getSubStringLength(0, i);
      // Remove 15 pixels from the width to fit the ellipsis
      if (textWidth < availableWidth) {
        this.text.textContent = `${tag.slice(0, i)}...`;
        return;
      }
    }
    // If nothing fits, remove the text
    this.text.textContent = '';
  }

  /**
   * The current position of the text element
   */
  textPos: Point = { x: 0, y: 0 } as Point;

  private get textX() {
    const computedTextLength = this.text.getComputedTextLength();
    const textEnd = this.currentViewX + computedTextLength;
    const rectEnd = this.rects[0].x + this.rects[0].width;
    let origin = this.rects[0].x as number;
    if (textEnd + TEXT_RIGHT_MARGIN >= rectEnd && textEnd > 0) {
      origin = rectEnd - computedTextLength - TEXT_RIGHT_MARGIN;
    } else if (this.rects[0].x < this.currentViewX) {
      origin = this.currentViewX;
    }
    return origin + TEXT_MARGIN;
  }

  @Watch('rects')
  @Watch('currentViewX')
  updateTextPos() {
    if (this.label.duration) {
      this.textPos.x = this.textX as Pixel;
      this.textPos.y = (this.rects[0].y + LABEL_TEXT_HEIGHT) as Pixel;
    } else {
      this.textPos.x = (this.rects[0].x + this.rects[0].width / 2) as Pixel;
      this.textPos.y = (this.rects[0].y + this.rects[0].height - 2) as Pixel;
    }
  }

  /**
   * The actual displayed rectangle
   * Can be smaller than rect to leave room for the moment handle
   * Can be smaller than rect to leave room for the bar text
   */
  displayRects: Rect[] = [{
    x: 0, y: 0, width: 0, height: 0,
  }] as Rect[];

  private cloneRects(rects: Rect[]) {
    return rects.map((rect) => ({ ...rect }));
  }

  @Watch('rects')
  updateDisplayRect() {
    this.displayRects = this.cloneRects(this.rects);
    if (!this.label.duration) {
      let vMargin = LABEL_TEXT_HEIGHT;
      if (this.isMomentBar) {
        this.displayRects[0].y = (this.rects[0].y + MOMENT_BAR_CIRCLE_DIAMETER) as Pixel;
        vMargin += MOMENT_BAR_CIRCLE_DIAMETER;
      }
      this.displayRects[0].height = (this.rects[0].height - vMargin) as Pixel;
    }
  }

  mounted() {
    this.onSelectionChange();
  }

  /**
   * When selection changes in the focused canvas, focus the selected label
   */
  @Watch('isSelected')
  onSelectionChange() {
    if (this.isSelected && this.isFocused) this.svgRect.focus({ preventScroll: true });
  }

  get isLabelDisabled(): boolean {
    return this.label.repeat !== undefined
      && this.label.repeat !== this.$store.state.synchro.currentRepeat;
  }

  get isGridBackground(): boolean {
    return this.sourceType === SourceType.Grid && this.label.type === 'Pattern';
  }

  get isGridHarmony(): boolean {
    return this.sourceType === SourceType.Grid && this.label.type === 'Harmony';
  }

  get isGridStructure(): boolean {
    return this.sourceType === SourceType.Grid && this.label.type === 'Structure';
  }

  get isGridGesture(): boolean {
    return this.sourceType === SourceType.Grid && this.label.type === 'Gesture';
  }
}
</script>
<style scoped lang="scss">

.label-rect{
  fill-opacity: 0.2;
  stroke-opacity: 0.2;
  stroke-width: 2;
  rx: 10px;
  ry: 10px;
  fill: var(--label-color, white);
  stroke: #000000;
}

// Selected label class, active when id === selectedId
.label-rect.selected{
  fill-opacity: 0.3;
  stroke-opacity: 1;
  stroke-width: 2;
}

// Remove the chrome rounded outline and the ff dash outline
// focus is different from selection, and synchronizing both is hard
.label-rect:focus{
  outline: none;
}

// Active when duration === 0
.label-rect.label-bar {
  fill-opacity: 0.3;
  stroke-opacity: 0.3;
}

// Moment class, active in duration mode when duration === 0
.label-rect.moment-bar {
  fill-opacity: 0.8;
  stroke-opacity: 0.8;
  rx: 2px;
  ry: 2px;
}

.label-text {
  color: black;
  opacity: 0.8;
  font-family: 'Roboto', 'Noto', sans-serif;
  font-size: 130%;
  font-weight: bold;
  user-select: none;
  pointer-events: none;
}

.label-text.selected {
  opacity: 1;
}

.label-text.label-bar {
  text-anchor: middle;
}

.label-icon {
  pointer-events: none;
  opacity: 95%;
}

.disabled > *, .disabled > * > * {
  fill: #33333355;
}

.grid-disabled {

  &-background > * {

    opacity: .5;

    & > text {
      display: none;
    }
    & > * {
      stroke-width: 0;
      pointer-events: none;
    }
  }

  &-harmony > * {
    & > text {
      font-size: 28px;
      fill: #444;
    }
    & > * {
      fill: #00000000;
      stroke-width: 0;

      pointer-events: none;
    }
  }

  &-structure > * {
    & > text {
      fill:#ddd;
      font-size: 32px;
    }
    & > * {
      fill: #00000000;
      stroke-width: 0;

      pointer-events: none;
    }
  }
}

.grid-gesture > g > rect {
  fill: #00000000;
  rx: 0px;
  ry: 0px;
}
</style>
