import Vue from 'vue';
import type { VuetifyDialog } from 'vuetify-dialog';
import type { Client } from '@/client';
import { ClientStatus } from '@/client';
import type { Analysis, EditableUnit } from '@/data';
import ExportDialog from '@/components/dialogs/ExportDialog.vue';
import type { Store } from 'vuex';
import type { DefaultStoreState } from '@/store/default.state';
import UploadDialog from './UploadDialog.vue';
import SyncUploadDialog from './SyncUploadDialog.vue';
import LoginDialog from './LoginDialog.vue';
import ChangePasswordDialog from './ChangePasswordDialog.vue';
import AboutDialog from './AboutDialog.vue';
import HelpDialog from './HelpDialog.vue';
import WarningDialog from './WarningDialog.vue';
import AddPieceDialog from './AddPieceDialog.vue';

export async function showLogin(client: Client): Promise<boolean> {
  const dialog: VuetifyDialog = Vue.prototype.$dialog;
  return dialog.showAndWait(LoginDialog, { client, persistent: false });
}

export async function showChangePassword(client: Client): Promise<boolean> {
  const dialog: VuetifyDialog = Vue.prototype.$dialog;
  return dialog.showAndWait(ChangePasswordDialog, { client, persistent: false });
}

async function showWarning(message: string, path = '', confirm = false, snack = false): Promise<boolean> {
  if (snack) {
    const store: Store<DefaultStoreState> = Vue.prototype.$store;
    const translated = Vue.prototype.i18n.t(`dialogs.warning.dialog.${message}.text`);
    store.dispatch('updateSnackText', translated);
    store.dispatch('showSnack');
    return false;
  }
  const dialog: VuetifyDialog = Vue.prototype.$dialog;

  const dialogObject = await dialog.show(WarningDialog, {
    persistent: true,
    message,
    confirm,
    path,
  });

  // If url change does occur despite the dialog (previous page button)
  //   Simply close the dialog
  window.addEventListener('popstate', () => {
    dialogObject.remove();
  }, { once: true });
  window.addEventListener('pushstate', () => {
    dialogObject.remove();
  }, { once: true });

  return dialogObject.wait();
}

export async function warnAnalysisUploadServerUnauth(): Promise<boolean> {
  return showWarning('warnAnalysisUploadServerUnauth');
}

export async function warnAnalysisUploadServer(): Promise<boolean> {
  return showWarning('warnAnalysisUploadServer');
}

export async function warnAnalysisUploadParsing(): Promise<boolean> {
  return showWarning('warnAnalysisUploadParsing');
}

export async function warnAnalysisUpload(): Promise<boolean> {
  return showWarning('warnAnalysisUpload');
}

export async function warnAnalysisMissing(path?: string): Promise<boolean> {
  return showWarning('warnAnalysisMissing', path);
}

export async function warnSyncUploadServerUnauth(): Promise<boolean> {
  return showWarning('warnSyncUploadServerUnauth');
}

export async function warnSyncUploadServer(): Promise<boolean> {
  return showWarning('warnSyncUploadServer');
}

export async function warnSyncUploadParsing(): Promise<boolean> {
  return showWarning('warnSyncUploadParsing');
}

export async function warnSyncUpload(): Promise<boolean> {
  return showWarning('warnSyncUpload');
}

export async function warnSyncMissing(path?: string): Promise<boolean> {
  return showWarning('warnSyncMissing', path);
}

export async function showUpload(
  client: Client,
  piecePath: string,
  analysis: Analysis,
): Promise<string | null> {
  const dialog: VuetifyDialog = Vue.prototype.$dialog;
  const res = await dialog.showAndWait(UploadDialog, {
    client, piecePath, analysis,
  });
  if (typeof res === 'string') {
    return res;
  }
  if (res.status === ClientStatus.Unauth) {
    warnAnalysisUploadServerUnauth();
  } else if (res.status === ClientStatus.NoResponse) {
    warnAnalysisUploadServer();
  } else if (res.status === ClientStatus.ParsingError) {
    warnAnalysisUploadParsing();
  } else if (res.status === ClientStatus.Other) {
    warnAnalysisUpload();
  }
  return null;
}

export async function showSyncUpload(
  client: Client,
  path: string,
  synchro: EditableUnit,
): Promise<string | null> {
  const dialog: VuetifyDialog = Vue.prototype.$dialog;
  const res = await dialog.showAndWait(SyncUploadDialog, {
    client,
    path,
    synchro,
  });
  if (res.status === ClientStatus.Ok) {
    return res;
  } if (res.status === ClientStatus.Unauth) {
    warnSyncUploadServerUnauth();
  } else if (res.status === ClientStatus.NoResponse) {
    warnSyncUploadServer();
  } else if (res.status === ClientStatus.ParsingError) {
    warnSyncUploadParsing();
  } else if (res.status === ClientStatus.Other) {
    warnSyncUpload();
  } else if (res.status === ClientStatus.Missing) {
    warnSyncMissing(path);
  }
  return null;
}

export async function showExport(
  filename: string,
): Promise<string | boolean> {
  const dialog: VuetifyDialog = Vue.prototype.$dialog;

  return dialog.showAndWait(ExportDialog, {
    filename,
  });
}

export async function showAbout(): Promise<boolean> {
  const dialog: VuetifyDialog = Vue.prototype.$dialog;
  return dialog.showAndWait(AboutDialog, { width: '800px', actions: true });
}

export async function showHelp(): Promise<boolean> {
  const dialog: VuetifyDialog = Vue.prototype.$dialog;
  return dialog.showAndWait(HelpDialog, { width: '800px', actions: true });
}

export async function warnPieceMissing(path?: string): Promise<boolean> {
  return showWarning('warnPieceMissing', path);
}

export async function warnPieceUnauth(path?: string): Promise<boolean> {
  return showWarning('warnPieceUnauth', path);
}

export async function warnPieceServer(): Promise<boolean> {
  return showWarning('warnPieceServer', '', false, true);
}

export async function warnPieceOther(): Promise<boolean> {
  return showWarning('warnPieceOther', '', false, true);
}

export async function warnAnalysisUnsaved(): Promise<boolean> {
  return showWarning('warnAnalysisUnsaved', '', true);
}

export async function warnAnalysisParsing(path?: string): Promise<boolean> {
  return showWarning('warnAnalysisParsing', path);
}

export async function warnAnalysisUnknown(path?: string): Promise<boolean> {
  return showWarning('warnAnalysisUnknown', path);
}

export async function warnSyncUnsaved(): Promise<boolean> {
  return showWarning('warnSyncUnsaved', '', true);
}

export async function warnSyncParsing(path?: string): Promise<boolean> {
  return showWarning('warnSyncParsing', path);
}

export async function warnSyncUnknown(path?: string): Promise<boolean> {
  return showWarning('warnSyncUnknown', path);
}

export async function warnYoutubeError(): Promise<boolean> {
  return showWarning('warnYoutubeError');
}

export async function warnAudioFileError(): Promise<boolean> {
  return showWarning('warnAudioFileError');
}

export async function warnCorpusDataMissing(path: string): Promise<boolean> {
  return showWarning('warnCorpusDataMissing', path);
}

export async function warnCorpusDataUnauth(path: string): Promise<boolean> {
  return showWarning('warnCorpusDataUnauth', path);
}

export async function warnCorpusDataServer(): Promise<boolean> {
  return showWarning('warnCorpusDataServer');
}

export async function warnCorpusDataOther(): Promise<boolean> {
  return showWarning('warnCorpusDataOther', '', false, true);
}

export async function showAddPiece(
  client: Client,
// eslint-disable-next-line @typescript-eslint/no-explicit-any
): Promise<any> {
  const dialog: VuetifyDialog = Vue.prototype.$dialog;
  return dialog.showAndWait(AddPieceDialog, { client });
}
