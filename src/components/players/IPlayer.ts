import type { Second } from '@/data';

export default interface IPlayer {
  setCurrentTime(value: Second): void;

  reset(): void;
}
