import type { StaffId } from './Staff';
import type { Duration, Onset } from './Unit';

export type LabelId = number;

/**
 * The properties shared by all labels and label templates
 */
interface LabelBase {
  type: string;
  tag: string;
}

export type LabelDuration = 'point' | 'ioi' | 'duration';

/**
 * The interface of the LabelCanvas `Label` object
 * Parsed from .dez files using LabelParser, and created by AnalysisActions at runtime
 * Contains all information specific to display a specific label relative to a piece
 */
export default interface Label extends LabelBase {
  onset: Onset;
  durationType: LabelDuration;
  duration: Duration;
  staffId: StaffId;
  wasStaffLoaded: boolean;
  type: string;
  tag: string;
  comment: string;
  layers: string[];
  readonly id: LabelId;
  readonly format?: unknown;
  repeat?: string;
  disabled?: boolean;
  isResizable?: boolean;
  symbol?: string;
  icon?: string;
}

/**
 * A simplified version of Label for cases where we only need Time data
 */
export interface LabelTime {
  onset: Onset;
  duration: Duration;
  playedIn: boolean;
  playedOut: boolean;
}

/**
 * A label template allows AnalysisActions to recreate a previous label
 * It may be an object containing only specific information, or another label
 * Therefore most properties are not garanteed to be there
 */
export type LabelTemplate = Readonly<LabelBase> & Partial<Label>;

/**
 * The result of LabelParser, and input to the Analysis constructor
 * Exists because the id only makes sense in the context of the Analysis,
 * The id is never parsed or outputted in any file
 */
export type LabelNoId = Omit<Label, 'id'>;

/**
 * A function that allows filtering of labels
 * Used in getNext and getPrev to obtain a specific label
 */
export type LabelPredicate = (label: Label) => boolean;

// Union of Label property keys, depending on their property type
// Used for some typechecking MusicalTimeField and AnalysisActions

export type LabelOnsetKey = 'onset' | 'duration';

export type LabelStringKey = 'type' | 'tag' | 'comment';

export type LabelIntKey = 'staffId';

export type LabelDurationTypeKey = 'durationType';

export type LabelKey = LabelOnsetKey | LabelStringKey | LabelIntKey | LabelDurationTypeKey | 'repeat';
