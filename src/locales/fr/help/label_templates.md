Les boutons d'étiquettes facilitent la création d'étiquettes identiques ou similaires.
Ils sont enregistrés avec vos analyses.

<b>Créer des boutons.</b>
Sélectionnez l'onglet <kbd>Ajouter +</kbd>.
Appuyez sur l'icône crayon pour modifier ces boutons.
Rentrez le tag du bouton souhaité dans l'éditeur,
et validez par <kbd>Entrée</kbd>
Des suggestions d'étiquettes apparaitront au-dessus de l'éditeur.

Dans l´éditeur:

- <kbd>Effacer</kbd> supprime le dernier bouton,
ou celui que vous avez sélectionné.

- <kbd>←</kbd> et <kbd>→</kbd> change la sélection.

- <kbd>virgule</kbd> créé un nouveau bouton avec le tag entré.

- <kbd>Entrée</kbd> créé un nouveau bouton avec le tag entré, et ferme l'éditeur.

<b>Utiliser des boutons d'étiquettes.</b>
Cliquez sur un bouton
(ou utilisez les raccourcis <kbd>1</kbd>/<kbd>2</kbd>/<kbd>3</kbd>/...),
puis placez votre étiquette
sur la partition/forme d'onde/spectrogramme.
Lorsqu'un fichier audio ou vidéo est disponible,
les boutons peuvent être utilisés durant la lecture.