Les étiquettes contiennent:
- Un **début**, et potentiellement une **durée** et une **fin**, soit en temps musical (nombre de mesures et de temps) ou en temps en secondes.
- Un **type** (catégorie)
- Un **tag** optionnel (texte court, idéalement pris dans un vocabulaire contrôlé)
- Un **commentaire** optionnel (texte libre)

Vous pouvez modifier les étiquettes à la souris ou bien dans l'onglet Modifier .

Une **analyse** est un ensemble d'étiquettes. Dans la barre de navigation, vous pouvez:
- Enregistrer l'analyse sur le cloud Dezrann
- Enregistrer l'analyse sur votre ordinateur (fichier .dez)
Une fois enregistrée, vous pouvez charger l'analyse à tout moment. 