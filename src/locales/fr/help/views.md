Les pièces ont généralement plusieurs vues, c'est-à-dire des visualisations de la musique comme des partitions, des formes d'onde ou des spectrogrammes.

Le bouton en haut à droite permet de changer les vues.

Il est possible d'ouvrir plusieurs vues en parallèle: le bouton + en bas à droite des vues ouvre une nouvelle vue.