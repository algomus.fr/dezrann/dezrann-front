import Vue from 'vue';
import type {
  Analysis, Piece, Staff, StaffId, Duration, Onset,

  Label, LabelId, LabelKey, LabelPredicate, LabelTemplate,
} from '@/data';
import {
  SourceType,

  StaffType, STAFF_ID_OFFSET,
} from '@/data';

import Fuse from 'fuse.js';
import { SortedLabels } from './SortedLabels';
import type AnalysisActions from './AnalysisActions';
import type { TypeFilter } from './LabelFilters';
import LabelFilters, { LabelFilterType } from './LabelFilters';

/**
 * All logical functions applied to an analysis
 */
export default class AnalysisActionsImpl implements AnalysisActions {
  piece: Piece;

  analysis: Analysis;

  editCount = 0;

  tagEditCount = 0;

  selectCount = 0;

  targetCount = 0;

  sortedLabels: SortedLabels;

  labelFilters: LabelFilters;

  selectedId: LabelId | null = null;

  fuse!: Fuse<Label>;

  searchResults: Array<Label> = [];

  constructor(piece: Piece, analysis: Analysis) {
    this.piece = piece;
    this.analysis = analysis;
    this.sortedLabels = new SortedLabels(analysis.labels);
    this.labelFilters = new LabelFilters(
      [...new Set(Object.values(analysis.labels).map((l) => l.type))],
      [...new Set(Object.values(analysis.labels).flatMap((l) => l.layers))],
    );
    this.fuse = new Fuse(this.sortedLabels.labels, {
      shouldSort: false,
      ignoreLocation: true,
      threshold: 0.0,
      useExtendedSearch: true,
      keys: ['comment', 'tag'],
    });
  }

  updateSearchString(searchString: string): void {
    this.searchResults = this.fuse.search(searchString).map(({ item }) => item);
  }

  select(id: LabelId | null, requestTarget = false): void {
    if (id === null) {
      this.selectedId = null;
      this.selectCount += 1;
      return;
    }

    const label = this.getLabel(id);

    // eslint-disable-next-line no-console
    if (!label) console.error(`No label found for id: ${id}`);

    this.selectedId = label?.id ?? null;
    this.selectCount += 1;
    if (this.selectedId !== null && requestTarget) this.targetCount += 1;
  }

  getSelected(): Label | null {
    return this.getLabel(this.selectedId);
  }

  getPrettySelected(): string | null {
    const label = this.getSelected();

    if (!label) return null;
    let pretty = `${label.onset}`;
    pretty += ` m${this.piece.signature.printOnset(label.onset)}`;
    pretty += ` ${this.piece.signature.printDuration(label.duration, label.onset)}`;
    pretty += ` ${label.type} ${label.tag}`;

    if (label.comment) {
      if (label.comment.includes('\n')) {
        pretty += `\n# ${label.comment.replaceAll('\n', '\n# ')}`;
      } else {
        pretty += ` # ${label.comment}`;
      }
    }

    return pretty;
  }

  deleteId(labelId: LabelId): void {
    const labelToDelete = this.getLabel(labelId);

    if (!labelToDelete) throw new Error('Label does not exist');

    if (labelId === this.selectedId) this.select(null);

    Vue.delete(this.analysis.labels, labelId);
    this.sortedLabels.remove(labelToDelete);

    this.editCount += 1;
    this.tagEditCount += 1;
  }

  updateLabel<K extends LabelKey>(
    id: LabelId,
    key: K,
    value: Label[K],
  ): void {
    const label = this.getLabel(id);

    if (!label) return;
    // this.sortedLabels.remove(label);
    label[key] = value;
    // this.sortedLabels.add(label);
    this.editCount += 1;

    if (key === 'type') {
      this.labelFilters.setOrCreate(value as string, true, LabelFilterType.Type);
    }

    if (key === 'tag') {
      label.icon = this.analysis.filters.icon(label.tag);
    }

    // * originally, instead of `sortedLabels.update` (O(n))
    // * we had `sortedLabels.remove` followed by `sortedLabels.add()`
    // * which could technically be O(2log(n)) with sparse arrays
    // * however this caused more issues than it was worth
    // * a sort on a nearly sorted array of less than 1000 elements is very cheap
    // * it is still, all in all, 4 times slower
    // * it could be even faster with a real update function that checks if the position has changed
    this.sortedLabels.update();

    if (key === 'tag') this.tagEditCount += 1;
  }

  template: LabelTemplate = { type: 'Pattern', tag: 'X', durationType: 'duration' };

  setTemplate(template: LabelTemplate): void {
    this.template = template;
  }

  getTemplate(): LabelTemplate {
    return this.template;
  }

  public createLabel(): LabelId {
    this.analysis.lastId += 1;
    const id = this.analysis.lastId;

    const newLabel: Label = {
      onset: 0 as Onset,
      duration: 0 as Duration,
      durationType: this.template?.durationType ?? 'duration',
      staffId: 0,
      wasStaffLoaded: true,
      type: this.template?.type ?? '',
      layers: ['no-layer'],
      tag: this.template?.tag ?? '',
      comment: '',
      format: null,
      id,
      isResizable: this.analysis.filters.styleType(this.template.tag, this.template.type) !== 'mini',
    };

    if (this.template?.tag) {
      newLabel.icon = this.analysis.filters.icon(this.template.tag);
    }

    this.labelFilters.setOrCreate(newLabel.type, true, LabelFilterType.Type);
    this.labelFilters.setOrCreate(newLabel.layers[0], true, LabelFilterType.Layer);

    Vue.set(this.analysis.labels, id, newLabel);
    this.editCount += 1;
    this.sortedLabels.add(newLabel);

    return id;
  }

  public updateTypesFilter(filters: TypeFilter): void {
    this.labelFilters.set(filters, LabelFilterType.Type);
  }

  public updateLayersFilter(filters: TypeFilter): void {
    this.labelFilters.set(filters, LabelFilterType.Layer);
  }

  public getFilterValueFor(filter: string, type: LabelFilterType): boolean {
    if (type === LabelFilterType.Type) {
      return this.labelFilters.types[filter];
    }
    return this.labelFilters.layers[filter];
  }

  public getSortedLabels(sourceType: SourceType = SourceType.Score): Label[] {
    const labels = this.searchResults.length ? this.searchResults : this.sortedLabels;
    const filteredLabels = labels
      .filter((label) => this.labelFilters
        .asStringArray(LabelFilterType.Type).includes(label.type))
      .filter((label) => this.labelFilters
        .asStringArray(LabelFilterType.Layer)
        .some((l) => label.layers.includes(l))
        || label.layers.length === 0);
    if (sourceType === SourceType.Grid) {
      // return filteredLabels.filter((l) => !['Harmony'].includes(l.type));
      return filteredLabels.map((l) => {
        if (l.type === 'Harmony') {
          return {
            ...l,
            staffId: 0,
            disabled: true,
          };
        }
        return l;
      });
    }
    return filteredLabels;
  }

  public getLabel(labelId: LabelId | null): Label | null {
    if (labelId === null) return null;

    const label = this.analysis.labels[labelId];

    // If no label is here, either deleteId was called on an expired id
    // Or labels are still in sortedLabels after having been removed
    if (!label) {
      // Ensure it's removed everywhere
      const removedLabels = this.sortedLabels.filter((l) => !this.analysis.labels[l.id]);
      // eslint-disable-next-line no-console
      if (removedLabels.length) console.error('Removing unsynced labels: ', removedLabels);
      this.sortedLabels.reset();
    }

    return label ?? null;
  }

  public getPrev(labelId: LabelId | null, predicate?: LabelPredicate): Label | null {
    // Get the label
    const label = this.getLabel(labelId);
    // Get the prev label
    return this.sortedLabels.prev(label, predicate);
  }

  public getNext(labelId: LabelId | null, predicate?: LabelPredicate): Label | null {
    // Get the label
    const label = this.getLabel(labelId);
    // Get the next label
    return this.sortedLabels.next(label, predicate);
  }

  deleteSelected(selectNext = false, requestTarget = false): Label | null {
    const id = this.selectedId;

    if (id === null) return null;

    if (selectNext) {
      const next = this.getNext(id) || this.getPrev(id) || null;
      if (next) this.select(next.id, requestTarget);

      this.deleteId(id);
      return next;
    }
    this.deleteId(id);
    return null;
  }

  findFreeStaff(onset: Onset, duration: Duration, staves: Staff[], hint?: StaffId): StaffId {
    // If no Analysis is present
    // Or no piece is not present
    // Or no sources are loaded
    // Return the first line
    if (!this.analysis || !this.piece || !this.piece.sources[0]) return 1;

    // Initialize data for the algorithm
    const labelsArray = Object.values(this.analysis.labels);
    const offset = onset + duration;

    // Count the number of times a label intersecs with the interval given
    function countIntersections(ls: Label[]) {
      return ls.filter(
        (l) => (l.onset < onset && onset < l.onset + l.duration)
            || (l.onset < offset && offset < l.onset + l.duration),
      ).length;
    }

    // Get the priority order of staves
    function staffPriority(staff: Staff): number {
      if (staff.id === hint) return 0;
      if (staff.type === StaffType.staff) return 1;
      return 2;
    }

    // For every staff that is not the bar staff
    const staffId = staves.filter((s) => s.type !== StaffType.bar)
      // Sort so the hint is first, and the real staves are before the lines
      .sort((s1, s2) => staffPriority(s1) - staffPriority(s2))
      // Get the id
      .map((s) => s.id)
      // Associate with its labels
      .map((id) => ({ id, labels: labelsArray.filter((l) => l.staffId === id) }))
      // Associate with the number of intersection with the given interval
      .map(({ id, labels }) => ({ id, score: countIntersections(labels) }))
      // Sort by number of intersections and get the first id
      .sort((pair1, pair2) => pair1.score - pair2.score)?.[0]?.id;

    return staffId ?? STAFF_ID_OFFSET;
  }
}
