export type TypeFilter = Record<string, boolean>;

export type LayerFilter = Record<string, boolean>;

export enum LabelFilterType {
  Type = 'type',
  Layer = 'layer'
}

export default class LabelFilters {
  constructor(typesArray: Array<string>, layersArray: Array<string>) {
    const types: TypeFilter = {};
    typesArray.forEach((k: string) => { types[k] = true; });
    this.types = types;

    const layers: TypeFilter = {};
    layersArray.forEach((k: string) => { layers[k] = true; });
    this.layers = layers;
  }

  public types: TypeFilter;

  layers: LayerFilter;

  set(elems: TypeFilter, type: LabelFilterType): void {
    if (type === LabelFilterType.Layer) {
      this.layers = elems;
    } else {
      this.types = elems;
    }
  }

  add(elem: string, type: LabelFilterType): void {
    if (type === LabelFilterType.Layer) {
      this.layers[elem] = true;
    } else {
      this.types[elem] = true;
    }
  }

  toggle(elem: string, type: LabelFilterType): void {
    if (type === LabelFilterType.Layer) {
      this.layers[elem] = !this.layers[elem];
    } else {
      this.types[elem] = !this.types[elem];
    }
  }

  setOrCreate(elem: string, value: boolean, type: LabelFilterType): void {
    if (type === LabelFilterType.Layer) {
      if (this.layers[elem] === undefined) {
        this.add(elem, type);
      } else {
        this.layers[elem] = value;
      }
    } else if (this.types[elem] === undefined) {
      this.add(elem, type);
    } else {
      this.types[elem] = value;
    }
  }

  hideAll(type: LabelFilterType): void {
    if (type === LabelFilterType.Layer) {
      Object.keys(this.layers).forEach((k) => {
        this.layers[k] = false;
      });
    } else {
      Object.keys(this.types).forEach((k) => {
        this.types[k] = false;
      });
    }
  }

  showAll(type: LabelFilterType): void {
    if (type === LabelFilterType.Layer) {
      Object.keys(this.layers).forEach((k) => {
        this.layers[k] = true;
      });
    } else {
      Object.keys(this.types).forEach((k) => {
        this.types[k] = true;
      });
    }
  }

  asStringArray(type: LabelFilterType): Array<string> {
    if (type === LabelFilterType.Layer) {
      return Object.keys(this.layers)
        .filter((k) => this.layers[k]);
    }
    return Object.keys(this.types)
      .filter((k) => this.types[k]);
  }
}
