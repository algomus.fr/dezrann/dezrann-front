import type { ActionTree } from 'vuex';
import type { SynchroStoreState } from './synchro.state';

export const actions: ActionTree<SynchroStoreState, SynchroStoreState> = {
  updateCurrentRepeat({ commit }, repeat: number) {
    commit('updateCurrentRepeat', repeat);
  },
};
