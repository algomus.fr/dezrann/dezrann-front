import type { MutationTree } from 'vuex';
import type { SynchroStoreState } from './synchro.state';

export const mutations: MutationTree<SynchroStoreState> = {
  updateCurrentRepeat(state: SynchroStoreState, value: string) {
    state.currentRepeat = value;
  },
};
