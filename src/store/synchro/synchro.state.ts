export interface SynchroStoreState {
  currentRepeat: string | undefined;
}

export const state = (): SynchroStoreState => ({
  currentRepeat: 'null',
});
