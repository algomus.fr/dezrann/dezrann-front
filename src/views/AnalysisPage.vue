<template>
  <v-main class="app">
    <piece-loader
      :client="client"
      :piece-path="piecePath"
      @update:piece="onPieceLoad"
    />
    <v-snackbar
      v-model="$store.state.snack.show"
      timeout="-1"
    >
      {{ $store.state.snack.text }}

      <template #action="{ attrs }">
        <v-btn
          color="pink"
          text
          v-bind="attrs"
          @click="$store.dispatch('hideSnack')"
        >
          Close
        </v-btn>
      </template>
    </v-snackbar>
    <!-- The main dezrann component -->
    <!-- Receives a piece and analysis, and handles display and editing -->
    <analysis-editor
      v-if="piece && analysis"
      :clean.sync="clean"
      :piece="piece"
      :analysis="analysis"
      :scroll.sync="scroll"
    />
    <!-- Renders analysis control buttons on the sidebar -->
    <portal v-slot="{ isMinified }" to="sidebar-buttons">
      <analysis-sidebar-items
        :piece="piece"
        :piece-path="piecePath"
        :client="client"
        :is-minified="isMinified"
        :clean.sync="clean"
        :analysis="analysis"
        :can-edit-sync="(allowSync && piece && piece.tracks.length > 0) ? true : false"
        @replace-analysis="onAnalysisReplace"
      />
    </portal>
    <title-manager
      v-if="!hideTitle && !loading"
      :clean.sync="clean"
      :index-current-piece="indexCurrentPiece"
      :corpus="corpus"
    />
    <scroll-query-manager
      v-if="piece"
      :scroll.sync="scroll"
      :signature="piece.signature"
    />
  </v-main>
</template>

<script lang="ts">
import { Component, Watch } from 'vue-property-decorator';
import type { Route } from 'vue-router';
import TitleManager from '@/components/navigation/TitleManager.vue';
import AnalysisSidebarItems from '@/components/navigation/analysis/AnalysisSidebarItems.vue';
import AnalysisEditor from '@/components/AnalysisEditor.vue';
import PieceLoader from '@/vuejs/components/loaders/PieceLoader.vue';
import ScrollQueryManager from '@/components/navigation/ScrollQueryManager.vue';
import {
  Analysis,
} from '@/data';
import type {
  Onset,
  Piece,
} from '@/data';
import {
  generateGridSource,
} from '@/client/sources';

import {
  warnAnalysisUnsaved,
  warnAnalysisMissing,
  warnAnalysisParsing,
  warnPieceServer,
  warnAnalysisUnknown,
} from '@/components/dialogs';

import type { Client } from '@/client';
import { ClientStatus } from '@/client';
import {
  extractQuery, getClient, getCorpus, getIndexPieceAlt, getPiecePath, removeLast, sortCorpus,
} from '@/vuejs/components/utils/nav-tools';
import CleanCheckPage from './CleanCheckPage.vue';

/**
 * The main dezrann integration
 * loads piece and analysis for the AnalysisEditor
 * then displays a piece analysis and lets a user modify it
 */
@Component({
  components: {
    AnalysisEditor,
    AnalysisSidebarItems,
    PieceLoader,
    ScrollQueryManager,
    TitleManager,
  },
})
export default class AnalysisPage extends CleanCheckPage {
  allowSync = !!process.env.VUE_APP_ALLOW_SYNC;

  // The data passed to the editor once loaded
  analysis: Analysis | null = null;

  piece: Piece | null = null;

  corpus: Piece[] = this.$store.state.corpus;

  indexCurrentPiece = this.$store.state.indexCurrentPiece;

  loading = true;

  /** The current piece path, to load the piece from the client */
  get piecePath(): string { return getPiecePath(this.$route); }

  /** The current client for this route */
  get client(): Client { return getClient(this.$route); }

  /** The current analysis id, to load the piece from the client */
  get analysisId(): string | null {
    return extractQuery(this.$route.query.analysis) ?? this.piece?.defaultAnalysis ?? null;
  }

  get hideTitle(): boolean {
    return 'secret' in this.$route.query;
  }

  get isGridAnalysis(): boolean {
    return this.piece?.analysisGrid !== null;
  }

  onAnalaysisUpdate() {
    // Generate and use grid
    if (this.analysis && this.piece) {
      // Build breakPoints at 'Structure' labels
      const labelKeys = Object.keys(this.analysis.labels);
      const breakPoints: Onset[] = [];
      const pageBreakPoints: Onset[] = [];
      for (let i = 0; i < labelKeys.length; i += 1) {
        const element = labelKeys[i];
        const label = this.analysis.labels[parseInt(element, 10)];
        if (label.type === 'Structure' && label.onset > 0) {
          breakPoints.push(this.analysis.labels[parseInt(element, 10)].onset);
        }
        if (label.type === 'Page' && label.onset > 0) {
          pageBreakPoints.push(this.analysis.labels[parseInt(element, 10)].onset);
        }
      }
      // TODO: #628, take measure map
      const measureCount = Math.ceil((this.piece.sources[0].pixelUnit.tables[0][
        this.piece.sources[0].pixelUnit.tables[0].length - 1
      ][0] as unknown as number) / 4);
      const source = generateGridSource(
        this.piece.signature,
        measureCount,
        4,
        breakPoints,
        pageBreakPoints,
      );
      this.piece.sources.push(source);
      if (this.analysis.fileName === this.piece.analysisGrid) {
        this.piece.settings.panels = [[this.piece.sources.length - 1]];
      }
    }
  }

  async onPieceLoad(piece: Piece) {
    this.piece = piece;
    this.analysis = await this.loadAnalysis(piece);
    if (this.isGridAnalysis) {
      this.onAnalaysisUpdate();
    }

    this.clean = true;
    if (removeLast(piece.path, '/') !== this.$store.state.corpusId) await this.updateCorpus();
    this.indexCurrentPiece = getIndexPieceAlt(piece, this.corpus);
    this.$store.dispatch('updateIndexCurrentPiece', { indexCurrentPiece: this.indexCurrentPiece });
    this.loading = false;
  }

  async updateCorpus() {
    const corpora = await this.client.loadCorpus();
    this.corpus = sortCorpus(await getCorpus(this.piece as Piece, corpora.data));
    this.$store.dispatch('updateCorpus', {
      corpus: this.corpus,
      corpusId: removeLast((this.piece as Piece).path, '/'),
    });
  }

  @Watch('$route.query.analysis', { immediate: true })
  async onAnalysisIdChange() {
    // If no piece is loaded, ignore change. analysis will be loaded by onPieceLoad
    // If no id is defined, ignore change. this was likely caused by onAnalysisReplace
    if (!this.piece || !this.analysisId) return;

    this.analysis = await this.loadAnalysis(this.piece);
    if (this.isGridAnalysis) {
      this.onAnalaysisUpdate();
    }
    this.clean = true;
  }

  async onAnalysisReplace(analysis: Analysis) {
    // Warn user if unsaved analysis, and return if they answer no
    if (!this.clean && await this.warnUser() === false) return;

    // Otherwise successful, replace analysis
    this.analysis = analysis;
    this.clean = true;

    // Try to remove the analysis id from the url
    this.$router.push({
      query: {
        ...this.$route.query,
        analysis: undefined,
      },
    });
  }

  async loadAnalysis(
    piece: Piece,
  ): Promise<Analysis> {
    const { analysisId } = this;

    if (!analysisId) return new Analysis([]);

    const analysisReq = await this.client.loadAnalysis(piece, analysisId);

    switch (analysisReq.status) {
      case ClientStatus.Ok: return analysisReq.data;
      case ClientStatus.NoResponse: warnPieceServer(); break;
      case ClientStatus.ParsingError: warnAnalysisParsing(analysisId); break;
      case ClientStatus.Missing: warnAnalysisMissing(analysisId); break;
      default: warnAnalysisUnknown(analysisId); break;
    }

    return new Analysis([]);
  }

  /**
   * @override
   * Warns the user in case of unsaved analysis on routing
   */
  warnUser = warnAnalysisUnsaved;

  /**
   * @override
   * Check if new route will erase current analysis
   * Use by CleanCheckPage
   */
  checkRouteChange(to: Route, from: Route): boolean {
    return to.query.analysis !== from.query.analysis;
  }

  /**
   * The current scroll of the editor
   */
  scroll: Onset | null = null;
}
</script>
