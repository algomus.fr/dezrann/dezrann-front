import { test, chromium } from '@playwright/test';
import { playAudit } from 'playwright-lighthouse';

test('Ligthouse performance test', async () => {
  test.setTimeout(0);
  const thresholds = {
    performance: 20,
    accessibility: 50,
    'best-practices': 50,
    seo: 50,
  };
  const browser = await chromium.launch({
    args: ['--remote-debugging-port=9222'],
    headless: true,
  });
  const page = await browser.newPage();
  await page.goto('/');
  await playAudit({
    page,
    thresholds,
    port: 9222,
    opts: {
      logLevel: 'error',
    },
    reports: {
      formats: {
        html: true, // defaults to false
      },
      name: `ligthouse-${new Date().toISOString()}`, // defaults to `lighthouse-${new Date().getTime()}`
      directory: `${process.cwd()}/lighthouse`, // defaults to `${process.cwd()}/lighthouse`
    },
  });
  await page.close();
  await browser.close();
});
