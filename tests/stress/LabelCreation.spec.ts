/* eslint-disable no-await-in-loop */
import test from '@playwright/test';
import * as fs from 'fs';
import createImage from './helper';

test('Stress', async ({ page, browser }) => {
  test.setTimeout(6 * 60 * 1000);

  const startDate = Date.now();
  let currentDate = Date.now();
  const dates = [];

  await page.goto('/~~/bwv847');
  await page.waitForSelector('.label-rect');

  const labels = await page.locator('.label-rect');
  const firstLabelBBox = await labels.first().boundingBox();
  if (!firstLabelBBox) return;
  const startX = firstLabelBBox.x + firstLabelBBox.width / 2;
  const startY = (firstLabelBBox.y + firstLabelBBox.height / 2) - firstLabelBBox.height;

  let i = 0;
  // eslint-disable-next-line no-constant-condition
  while (currentDate - startDate <= 5 * 60 * 1000) {
    const lastDate = Date.now();
    // Create New Label
    await page.mouse.move(startX, startY);
    await page.mouse.down();
    await page.mouse.move(startX + 10, startY);
    await page.mouse.up();

    // Move Last Label
    await page.mouse.move(startX - 5, startY);
    await page.mouse.down();
    await page.mouse.move(startX + 300, startY);
    await page.mouse.up();
    currentDate = Date.now();
    dates.push({
      iteration: i += 1,
      time: currentDate - lastDate,
    });
  }

  if (!fs.existsSync('stresstest-results')) fs.mkdirSync('stresstest-results');
  fs.writeFileSync(
    `stresstest-results/dates-create-${browser.browserType().name()}.svg`,
    await createImage(dates),
  );
  fs.writeFileSync(`stresstest-results/dates-create-${browser.browserType().name()}.json`, JSON.stringify(dates));
});
