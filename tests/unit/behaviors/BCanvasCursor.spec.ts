import BCanvasCursor from '@/components/behaviors/player/BCanvasCursor.vue';
import 'vuetify';
import {
  getFakePixels, initBehaviorContext,
} from './utils';

describe('BCanvasCursor.vue', () => {
  it('check that cursor follows musical Time', async () => {
    // Get fake behavior context
    const {
      behavior,
    } = initBehaviorContext(BCanvasCursor, {
      pixelUnit: getFakePixels(),
      musicalTime: null,
      realTime: null,
    });

    // Expect default cursor position to be hidden
    expect(behavior.isVisible()).toBe(false);
    expect(behavior.vm.position).toBe(null);

    // Expect musicalTime:0 then x: 0
    await behavior.setProps({ musicalTime: 0 });
    expect(behavior.find('.canvas-cursor').isVisible()).toBe(true);
    expect(behavior.vm.position).toStrictEqual({ page: 0, row: 0, x: 0 });

    // Test for others coordinates using the fake pixel unit (x10)
    await behavior.setProps({ musicalTime: 5 });
    expect(behavior.find('.canvas-cursor').isVisible()).toBe(true);
    expect(behavior.vm.position).toStrictEqual({ page: 0, row: 0, x: 50 });

    await behavior.setProps({ musicalTime: 10 });
    expect(behavior.find('.canvas-cursor').isVisible()).toBe(true);
    expect(behavior.vm.position).toStrictEqual({ page: 0, row: 0, x: 100 });

    await behavior.setProps({ musicalTime: 20 });
    expect(behavior.find('.canvas-cursor').isVisible()).toBe(true);
    expect(behavior.vm.position).toStrictEqual({ page: 0, row: 0, x: 100 });

    // Check actual pixel position
    await behavior.setProps({ musicalTime: 5 });
    const cursorAttr = behavior.find('.canvas-cursor').attributes();

    const realPos = Number.parseInt(cursorAttr.x, 10) + Number.parseInt(cursorAttr.width, 10) / 2;
    expect(realPos).toBe(50);

    // Check the behavior moves back to hidden
    await behavior.setProps({ musicalTime: null });
    expect(behavior.isVisible()).toBe(false);

    // TODO: test realTime prop
  });
});
