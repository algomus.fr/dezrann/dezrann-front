import BCopyPasteOnKey from '@/components/behaviors/shared/BCopyPasteOnKey.vue';
import 'vuetify';
import { initBehaviorContext } from './utils';

describe('BCopyPasteOnKey.vue', () => {
  // Mocks clipboard
  const originalClipboard = { ...global.navigator.clipboard };

  beforeAll(() => {
    let clipboardContents = '';
    Object.assign(navigator, {
      clipboard: {
        writeText: jest.fn((text) => { clipboardContents = text; }),
        readText: jest.fn(() => clipboardContents),
      },
    });
  });

  afterAll(() => {
    Object.assign(navigator, { clipboard: originalClipboard });
  });

  it('check copying labels to clipboard', async () => {
    const {
      testEmit, actions,
    } = initBehaviorContext(BCopyPasteOnKey, {
      playing: false,
    });

    // Mocks the selection and the export
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    actions.getSelected = jest.fn(() => ({ id: 0, type: 'foo', onset: 4 })) as any;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    actions.getPrettySelected = jest.fn(() => ('pretty')) as any;

    // Nothing in the clipboard
    testEmit('document-keydown', { key: 'Enter' });
    expect(navigator.clipboard.readText()).toEqual('');

    // After Ctrl-C, label in the clipboard (pretty)
    testEmit('document-ctrl-keydown', { key: 'c', ctrlKey: true });
    expect(navigator.clipboard.readText()).toEqual('pretty');

    // After j, label in the clipboard (json)
    testEmit('document-keydown', { key: 'j' });
    expect(JSON.parse(await navigator.clipboard.readText())).toEqual({ start: 4, type: 'foo' });
  });
});
