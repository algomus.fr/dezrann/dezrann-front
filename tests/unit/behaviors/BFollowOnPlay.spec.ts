import BFollowOnPlay from '@/components/behaviors/player/BFollowOnPlay.vue';
import 'vuetify';
import type { Rect } from '@/data';
import { initBehaviorContext } from './utils';

const PIXEL_MARGIN = 10;

describe('BFollowOnPlay.vue', () => {
  it('check that follow starts at the right moments', async () => {
    // Fake the scrollTo function
    const scrollTo = jest.fn();

    const {
      behavior,
      testEmit,
    } = initBehaviorContext(BFollowOnPlay, {
      scrollTo,
      playing: false,
      margin: PIXEL_MARGIN,
      viewRect: {
        x: 0, y: 0, width: 50, height: 10,
      } as Rect,
    });

    expect(behavior.vm.following).toBe(false);

    // Check that starting to play out of bounds does not cause follow
    await behavior.setProps({ playing: true });
    await testEmit('cursor-move', { x: 50, y: 0 });
    expect(behavior.vm.following).toBe(false);

    expect(scrollTo).not.toHaveBeenCalled();

    // Reset the play
    await behavior.setProps({ playing: false });

    // Check that starting the play in bounds cause a follow
    await behavior.setProps({ playing: true });
    await testEmit('cursor-move', { x: 0, y: 0 });
    expect(behavior.vm.following).toBe(true);

    expect(scrollTo).not.toHaveBeenCalled();

    // Check that follow is actually called
    await testEmit('cursor-move', { x: 100, y: 0 });
    expect(behavior.props().margin).toBe(PIXEL_MARGIN);
    const expectedScroll = 100 - PIXEL_MARGIN;

    // Update the scroll as if scrollTo had really been called
    await behavior.setProps({
      viewRect: {
        x: expectedScroll, y: 0, width: 50, height: 10,
      },
    });
    // Check that scrolling back stops the follow
    await behavior.setProps({
      viewRect: {
        x: 0, y: 0, width: 50, height: 10,
      },
    });
    expect(behavior.vm.following).toBe(false);

    expect(scrollTo).toHaveBeenLastCalledWith({ x: expectedScroll, y: 0 });
  });
});
