import type { Wrapper } from '@vue/test-utils';
import BMinimapCursor from '@/components/behaviors/scroll/BMinimapCursor.vue';
import type { Pixel, Rect } from '@/data';
import 'vuetify';
import {
  getFakePixels, initBehaviorContext,
} from './utils';

function checkRect(behavior: Wrapper<Vue>, i: number, rect: Rect, height: number) {
  const svgRect = behavior.findAll('.minimap-cursor').at(i);
  const attr = svgRect.attributes();

  expect(svgRect.isVisible()).toBe(true);
  expect(attr.x).toBe(rect.x.toString());
  expect(attr.y).toBe(rect.y.toString());
  expect(attr.width).toBe(rect.width.toString());
  expect(attr.height).toBe(height.toString());
}

function checkAllRects(behavior: Wrapper<Vue>, rect: Rect, height: number) {
  expect(behavior.findAll('.minimap-cursor').length).toBe(3);
  checkRect(behavior, 0, {
    ...rect, x: 0, width: rect.x,
  } as Rect, height);
  checkRect(behavior, 1, rect, height);
  checkRect(behavior, 2, {
    ...rect, x: rect.x + rect.width, width: 100000,
  } as Rect, height);
}

describe('BMinimapCursor.vue', () => {
  it('check that cursor follows viewRect', async () => {
    let viewRect = {
      x: 0, y: 0, width: 10, height: 10,
    } as Rect;
    // Get fake behavior context
    const {
      behavior,
    } = initBehaviorContext(BMinimapCursor, {
      pixelUnit: getFakePixels(),
      viewRect,
    });

    // Check starting pos

    // Test for others coordinates using the fake pixel unit (x10)
    viewRect = { ...viewRect, x: 50 as Pixel };
    const height = 10;
    await behavior.setProps({ viewRect });
    await behavior.setProps({ height });
    checkAllRects(behavior, viewRect, height);

    viewRect = { ...viewRect, x: 100 as Pixel };
    await behavior.setProps({ viewRect });
    checkAllRects(behavior, viewRect, height);

    viewRect = { ...viewRect, x: 200 as Pixel };
    await behavior.setProps({ viewRect });
    checkAllRects(behavior, viewRect, height);
  });
});
