import BMoveOnDrag from '@/components/behaviors/analysis/BMoveOnDrag.vue';
import 'vuetify';
import type {
  Duration, Label, Onset, Point,
} from '@/data';
import { STAFF_ID_OFFSET } from '@/data';
import type { BehaviorContext } from './utils';
import {
  getFakePixels,
  initBehaviorContext,
  getFakeStaves,
  expectAction,
} from './utils';

describe('BMoveOnDrag.vue', () => {
  // The shared variables for testing
  const scrollTo = jest.fn();
  let behaviorContext: BehaviorContext;
  const staves = getFakeStaves();
  const staff = staves[STAFF_ID_OFFSET];
  const label: Label = {
    id: 0,
    comment: '',
    duration: 5 as Duration,
    durationType: 'ioi',
    onset: 0 as Onset,
    layers: [],
    staffId: staff.id,
    tag: '',
    type: '',
    wasStaffLoaded: false,
  };

  // The init sequence
  beforeEach(() => {
    behaviorContext = initBehaviorContext(BMoveOnDrag, {
      pixelUnit: getFakePixels(),
      staves,
      snap: true,
    });
  });

  // The reset sequence
  afterEach(() => {
    scrollTo.mockClear();
  });

  it('check that drag starts uninit', async () => {
    const { behavior } = behaviorContext;
    expect(behavior.vm.state).toStrictEqual(null);
  });

  it('check that horizontal drag inits on label mousedown', async () => {
    const { testEmit, behavior, actions } = behaviorContext;
    await testEmit('label-mousedown', { x: 0, y: 0 } as Point, label, staff);
    expect(behavior.vm.state).toBeTruthy();
    expect(actions.updateLabel).toBeCalledTimes(0);
  });

  it('check that horizontal drag resets on mouseup', async () => {
    const { testEmit, behavior, actions } = behaviorContext;
    await testEmit('label-mousedown', { x: 0, y: 0 } as Point, label, staff);
    expect(behavior.vm.state).toBeTruthy();
    await testEmit('document-mouseup', { x: 0, y: 0 } as Point);
    expect(behavior.vm.state).toBeNull();
    expect(actions.updateLabel).toBeCalledTimes(0);
  });

  it('check that horizontal drag works', async () => {
    const { testEmit, actions } = behaviorContext;
    await testEmit('label-mousedown', { x: 0, y: 0 } as Point, label, staff);
    await testEmit('all-mousemove', { x: 50, y: 0 } as Point);
    expectAction(actions.updateLabel, [
      [label.id, 'onset', 5],
      [label.id, 'duration', 5],
    ]);
  });

  it('check that vertical drag works', async () => {
    const { testEmit, actions } = behaviorContext;
    await testEmit('label-mousedown', { x: 0, y: 0 } as Point, label, staff);
    await testEmit('staff-mousemove', { x: 50, y: 0 } as Point, staves[staff.id - 1]);
    expectAction(actions.updateLabel, [
      [label.id, 'staffId', staff.id - 1],
    ]);
  });

  it('check that horizontal drag respects right boundary', async () => {
    const { testEmit, actions } = behaviorContext;
    await testEmit('label-mousedown', { x: 0, y: 0 } as Point, label, staff);
    await testEmit('all-mousemove', { x: 50, y: 0 } as Point);
    expectAction(actions.updateLabel, [
      [label.id, 'onset', 5],
      [label.id, 'duration', 5],
    ]);

    await testEmit('all-mousemove', { x: 70, y: 0 } as Point);
    expectAction(actions.updateLabel, [
      [label.id, 'onset', 7],
      [label.id, 'duration', 3],
    ]);

    await testEmit('all-mousemove', { x: 200, y: 0 } as Point);
    expectAction(actions.updateLabel, [
      [label.id, 'onset', 10],
      [label.id, 'duration', 0],
    ]);
  });

  it('check that right horizontal drag is not lossy', async () => {
    const { testEmit, actions } = behaviorContext;
    await testEmit('label-mousedown', { x: 0, y: 0 } as Point, label, staff);
    await testEmit('all-mousemove', { x: 200, y: 0 } as Point);
    expectAction(actions.updateLabel, [
      [label.id, 'onset', 10],
      [label.id, 'duration', 0],
    ]);

    await testEmit('all-mousemove', { x: 0, y: 0 } as Point);
    expectAction(actions.updateLabel, [
      [label.id, 'onset', 0],
      [label.id, 'duration', 5],
    ]);
  });

  it('check that horizontal drag respects left boundary', async () => {
    const { testEmit, actions } = behaviorContext;
    await testEmit('label-mousedown', { x: 0, y: 0 } as Point, label, staff);
    await testEmit('all-mousemove', { x: -10, y: 0 } as Point);
    expectAction(actions.updateLabel, [
      [label.id, 'onset', 0],
      [label.id, 'duration', 4],
    ]);

    await testEmit('all-mousemove', { x: -50, y: 0 } as Point);
    expectAction(actions.updateLabel, [
      [label.id, 'onset', 0],
      [label.id, 'duration', 0],
    ]);

    await testEmit('all-mousemove', { x: -100, y: 0 } as Point);
    expectAction(actions.updateLabel, [
      [label.id, 'onset', 0],
      [label.id, 'duration', 0],
    ]);
  });

  it('check that horizontal drag respects left boundary when first onset is -1', async () => {
    const pixelUnit = getFakePixels();

    // Move onsets from range 0 -> 10 to 0 -> 9
    pixelUnit.tables[0] = pixelUnit.tables[0].map(
      ([onset, p]) => [onset - 1, p],
    );

    behaviorContext = initBehaviorContext(BMoveOnDrag, {
      pixelUnit,
      staves,
    });

    const { testEmit, actions } = behaviorContext;
    await testEmit('label-mousedown', { x: 0, y: 0 } as Point, label, staff);
    await testEmit('all-mousemove', { x: -10, y: 0 } as Point);
    expectAction(actions.updateLabel, [
      [label.id, 'onset', -1],
      [label.id, 'duration', 5],
    ]);

    await testEmit('all-mousemove', { x: -50, y: 0 } as Point);
    expectAction(actions.updateLabel, [
      [label.id, 'onset', -1],
      [label.id, 'duration', 1],
    ]);

    await testEmit('all-mousemove', { x: -100, y: 0 } as Point);
    expectAction(actions.updateLabel, [
      [label.id, 'onset', -1],
      [label.id, 'duration', 0],
    ]);

    await testEmit('all-mousemove', { x: 0, y: 0 } as Point);
    expectAction(actions.updateLabel, [
      [label.id, 'onset', 0],
      [label.id, 'duration', 5],
    ]);
  });

  it('check that left horizontal drag is not lossy', async () => {
    const { testEmit, actions } = behaviorContext;
    await testEmit('label-mousedown', { x: 0, y: 0 } as Point, label, staff);
    await testEmit('all-mousemove', { x: -50, y: 0 } as Point);
    expectAction(actions.updateLabel, [
      [label.id, 'onset', 0],
      [label.id, 'duration', 0],
    ]);

    await testEmit('all-mousemove', { x: 0, y: 0 } as Point);
    expectAction(actions.updateLabel, [
      [label.id, 'onset', 0],
      [label.id, 'duration', 5],
    ]);
  });

  it('check that snap works', async () => {
    const { testEmit, actions, behavior } = behaviorContext;

    await behavior.setProps({ snap: true });
    await testEmit('label-mousedown', { x: 0, y: 0 } as Point, label, staff);

    await testEmit('all-mousemove', { x: 41, y: 0 } as Point);
    expectAction(actions.updateLabel, [
      [label.id, 'onset', 4],
      [label.id, 'duration', 5],
    ]);

    await testEmit('all-mousemove', { x: 45, y: 0 } as Point);
    expectAction(actions.updateLabel, [
      [label.id, 'onset', 4],
      [label.id, 'duration', 5],
    ]);

    await testEmit('all-mousemove', { x: 46, y: 0 } as Point);
    expectAction(actions.updateLabel, [
      [label.id, 'onset', 5],
      [label.id, 'duration', 5],
    ]);
  });

  it('check that no snap works', async () => {
    const { testEmit, actions, behavior } = behaviorContext;

    await behavior.setProps({ snap: false });
    await testEmit('label-mousedown', { x: 0, y: 0 } as Point, label, staff);

    await testEmit('all-mousemove', { x: 41, y: 0 } as Point);
    expectAction(actions.updateLabel, [
      [label.id, 'onset', 4.1],
      [label.id, 'duration', 5],
    ]);

    await testEmit('all-mousemove', { x: 45, y: 0 } as Point);
    expectAction(actions.updateLabel, [
      [label.id, 'onset', 4.5],
      [label.id, 'duration', 5],
    ]);

    await testEmit('all-mousemove', { x: 46, y: 0 } as Point);
    expectAction(actions.updateLabel, [
      [label.id, 'onset', 4.6],
      [label.id, 'duration', 5],
    ]);
  });
});
