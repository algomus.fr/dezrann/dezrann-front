import BSelectOnClick from '@/components/behaviors/shared/BSelectOnClick.vue';
import 'vuetify';
import type { Label } from '@/data';
import { getFakeStaff, initBehaviorContext } from './utils';

describe('BSelectOnClick.vue', () => {
  it('check emitting ids of clicked labels', async () => {
    // Mock a component event source
    const staff = getFakeStaff();

    const { testEmit, actions } = initBehaviorContext(BSelectOnClick, {});

    testEmit('label-mousedown', undefined, { id: 0 } as Label, staff);

    expect(actions.select).toHaveBeenLastCalledWith(0);

    testEmit('label-mousedown', undefined, { id: 101 } as Label, staff);

    expect(actions.select).toHaveBeenLastCalledWith(101);
  });
});
