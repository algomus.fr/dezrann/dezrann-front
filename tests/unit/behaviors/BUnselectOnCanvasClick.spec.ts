import BUnselectOnCanvasClick from '@/components/behaviors/shared/BUnselectOnCanvasClick.vue';
import 'vuetify';
import { getFakeStaff, initBehaviorContext } from './utils';

describe('BUnselectOnCanvasClick.vue', () => {
  it('check emitting null on canvas-click', async () => {
    const staff = getFakeStaff();

    const { testEmit, actions } = initBehaviorContext(BUnselectOnCanvasClick, {});

    testEmit('staff-click', undefined, staff);

    expect(actions.select).toHaveBeenLastCalledWith(null);
  });
});
