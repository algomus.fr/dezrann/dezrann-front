import Vue from 'vue';
import Vuetify from 'vuetify';

import type { FunctionalComponentShallowMountOptions, Wrapper } from '@vue/test-utils';
import { mount } from '@vue/test-utils';
import ChangePasswordDialog from '@/components/dialogs/ChangePasswordDialog.vue';
import VueI18n from 'vue-i18n';
import VuetifyDialog from 'vuetify-dialog';

// Init vuetify, i18n and vuetify dialogs
// TODO: This should use localVue, like in LabelField.spec.ts
// however but it's hard to use it for vuetify and vuetifyDialog

Vue.use(VueI18n);
Vue.use(Vuetify);

const vuetify = new Vuetify();

const messages = {
  fr: {
    dialogs: {

      'change-password': {
        title: 'titleFr',
        oldPassword: 'oldPasswordFr',
        newPassword: 'newPasswordFr',
        confirmedPassword: 'confirmedPasswordFr',
        cancel: 'cancelFr',
        passwordsMustMatch: 'passwordsMustMatchFr',
        tooShortPassword: 'tooShortPasswordFr',
        unauthError: 'unauthErrorFr',
        serverUnreachable: 'serverUnreachableFr',
        unknownError: 'unknownErrorFr',
      },
    },
  },
};
const i18n = new VueI18n({
  locale: 'fr',
  messages,
});

const vuetifyDialogOptions = {
  context: { vuetify, i18n },
};

Vue.use(VuetifyDialog, vuetifyDialogOptions);

interface DialogVmType {
  vm :{
    loading: boolean;
    isValid: boolean;
    showOldPassword: boolean;
    showNewPassword: boolean;
    showConfirmedPassword: boolean;
    oldPassword: string;
    newPassword: string;
    confirmedPassword: string;
    serverMessage: string | null;
  }
}

type DialogType = Wrapper<ChangePasswordDialog>& DialogVmType;

interface DialogFindResult {
  dialog: DialogType;
  oldPasswordField: Wrapper<Vue>;
  confirmedPasswordField: Wrapper<Vue>;
  newPasswordField: Wrapper<Vue>;
  form: Wrapper<Vue>
}

function findComps(comp: DialogType): DialogFindResult {
  const dialog = comp;
  const oldPasswordField = comp.getComponent({ ref: 'oldPassword' });
  const newPasswordField = comp.getComponent({ ref: 'newPassword' });
  const confirmedPasswordField = comp.getComponent({ ref: 'confirmedPassword' });
  const form = comp.getComponent({ name: 'VForm' });

  return {
    dialog,
    oldPasswordField,
    confirmedPasswordField,
    newPasswordField,
    form,
  };
}

function mountComp(options: FunctionalComponentShallowMountOptions<Vue> = {}): DialogFindResult {
  const comp = mount(ChangePasswordDialog, { i18n, vuetify, ...options });
  return findComps(comp as DialogType);
}

const changePasswordSuccess = jest.fn(
  () => Promise.resolve(
    { isOk: () => true },
  ),
);

describe('test', () => {
  it('dialog is mounted', async () => {
    const {
      oldPasswordField,
      confirmedPasswordField,
      newPasswordField,
    } = mountComp();

    expect(oldPasswordField.text()).toContain(messages.fr.dialogs['change-password'].oldPassword);
    expect(confirmedPasswordField.text()).toContain(messages.fr.dialogs['change-password'].confirmedPassword);
    expect(newPasswordField.text()).toContain(messages.fr.dialogs['change-password'].newPassword);
  });

  it('expect password match to be detected, starting with confirmed', async () => {
    const {
      newPasswordField, confirmedPasswordField, dialog,
    } = mountComp();

    await confirmedPasswordField.get('input').setValue('test test');
    expect(dialog.vm.isValid).toBe(false);
    await Vue.nextTick();
    expect(confirmedPasswordField.text()).toContain(messages.fr.dialogs['change-password'].passwordsMustMatch);

    await newPasswordField.get('input').setValue('test test');
    await Vue.nextTick();
    expect(dialog.vm.isValid).toBe(true);
    await Vue.nextTick();
    expect(confirmedPasswordField.text()).not.toContain(messages.fr.dialogs['change-password'].passwordsMustMatch);
  });

  it('expect password match to be detected, starting with new', async () => {
    const {
      newPasswordField, confirmedPasswordField, dialog,
    } = mountComp();

    // Init the validation for password confirm
    await confirmedPasswordField.get('input').setValue('invalid');
    await newPasswordField.get('input').setValue('test test');
    expect(dialog.vm.isValid).toBe(false);
    await Vue.nextTick();
    expect(confirmedPasswordField.text()).toContain(messages.fr.dialogs['change-password'].passwordsMustMatch);

    await confirmedPasswordField.get('input').setValue('test test');
    await Vue.nextTick();
    expect(dialog.vm.isValid).toBe(true);
    await Vue.nextTick();
    expect(confirmedPasswordField.text()).not.toContain(messages.fr.dialogs['change-password'].passwordsMustMatch);
  });

  it('expect password too short to be detected', async () => {
    const {
      newPasswordField, dialog,
    } = mountComp();

    // Init the validation for password confirm
    await newPasswordField.get('input').setValue('test');
    expect(dialog.vm.isValid).toBe(false);
    await Vue.nextTick();
    expect(newPasswordField.text()).toContain(messages.fr.dialogs['change-password'].tooShortPassword);

    await newPasswordField.get('input').setValue('test test');
    await Vue.nextTick();
    expect(newPasswordField.text()).not.toContain(messages.fr.dialogs['change-password'].tooShortPassword);
  });

  const TEST_EMAIL = 'test@test.com';
  const TEST_OLD_PASSWORD = 'test test 1';
  const TEST_NEW_PASSWORD = 'test test 1';

  it('expect client to be called', async () => {
    const client = {
      changePassword: changePasswordSuccess,
      user: {
        name: TEST_EMAIL,
      },
    };

    const {
      oldPasswordField,
      newPasswordField,
      confirmedPasswordField,
      dialog,
    } = mountComp({ propsData: { client } });

    // Init the validation for password confirm
    await oldPasswordField.get('input').setValue(TEST_OLD_PASSWORD);
    await newPasswordField.get('input').setValue(TEST_NEW_PASSWORD);
    await confirmedPasswordField.get('input').setValue(TEST_NEW_PASSWORD);

    await Vue.nextTick();

    expect(dialog.vm.isValid).toBe(true);

    await dialog.get('button[action-key="login"]').trigger('click');

    await Vue.nextTick();

    expect(client.changePassword).toHaveBeenLastCalledWith(
      TEST_EMAIL,
      TEST_OLD_PASSWORD,
      TEST_NEW_PASSWORD,
    );
  });
});
