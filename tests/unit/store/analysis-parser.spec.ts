/* eslint-disable @typescript-eslint/no-var-requires */
import type { AnalysisFormat } from '@/formats/analysis-formats';
import { LineName } from '@/formats/analysis-formats';
import AnalysisParser from '@/formats/AnalysisParser';

const analysis: AnalysisFormat = {
  labels: [
    {
      start: 8.5,
      ioi: 7.5,
      type: 'S',
      tag: 'S',
      staff: '1',
    },
    {
      start: 40.5,
      duration: 7.5,
      type: 'S',
      tag: 'S',
      staff: '1',
    },
    {
      start: 56,
      duration: 8,
      type: 'CS1',
      tag: 'CS1',
      staff: '1',
    },
    {
      start: 60,
      type: 'Foo',
      line: 'top.1' as LineName,
    },
    {
      start: 102.25,
      type: 'CS1',
      tag: 'CS1a',
      staff: '1',
    },
    {
      start: 110,
      type: 'Pattern',
      tag: 'Predicted',
      layers: ['predicted'],
    },
    {
      start: 114,
      type: 'Cadence',
      tag: ' I:PAC',
    },
    {
      start: 114,
      type: 'Pedal',
      tag: 'Tonic pedal',
    },
    {
      start: 16,
      duration: 8,
      type: 'Structure',
      tag: 'codetta',
    },
    {
      start: 84,
      duration: 18,
      type: 'Structure',
      tag: 'episode',
    },
  ],
  meta: {
    date: '2021-01-06T13:50:38.306Z',
    producer: 'Dezrann c6c80150',
    title: 'Fugue in C minor',
    layout: [
      {
        filter: {
          type: 'Structure',
        },
        style: {
          line: LineName.bot1,
        },
      },
      {
        filter: {
          type: 'Pedal',
        },
        style: {
          line: LineName.bot2,
          color: '#bbbbbb',
        },
      },
      {
        filter: {
          type: 'Pattern',
          layers: [
            'predicted',
          ],
        },
        style: {
          line: LineName.bot3,
        },
      },
    ],
  },
};

/**
 * Describes the behavior of the AnalysisParser object
 */
describe('AnalysisParser', () => {
  it('=> parser/export should be reversible', () => {
    expect(analysis).toEqual(
      AnalysisParser.exportFormat(
        AnalysisParser.parseFormat(analysis),
        false,
      ),
    );
  });

  it('=> parser/export should be reversible while sorting the labels', () => {
    const sortedAnalysis = {
      labels: [...analysis.labels]
        .sort((a, b) => (a?.start as number ?? 0) - (b?.start as number ?? 0)),
      meta: analysis.meta,
    };

    expect(sortedAnalysis).toEqual(
      AnalysisParser.exportFormat(
        AnalysisParser.parseFormat(analysis),
      ),
    );
  });
});
