/* eslint-disable @typescript-eslint/no-var-requires */
import type {
  ImageSyncFormat, AudioSyncFormat, AudioImageSyncFormat, Image3DSyncFormat,
} from '@/formats/UnitParser';
import UnitFormat from '@/formats/UnitParser';
import type {
  Duration, Onset, Pixel,
  Second,
} from '@/data';

const SynchroRepeats = [
  { date: 1, onset: 1 },
  { date: 99, onset: 99 },
  { date: 100, onset: 100, repeat: 'a' },
  { date: 149, onset: 149, repeat: 'a' },
  // More points in 'b' than in 'a', but dichotomy still works
  { date: 150, onset: 100, repeat: 'b' },
  { date: 160, onset: 103, repeat: 'b' },
  { date: 170, onset: 110, repeat: 'b' },
  { date: 180, onset: 120, repeat: 'b' },
  { date: 188, onset: 123, repeat: 'b' },
  { date: 199, onset: 149, repeat: 'b' },
  { date: 200, onset: 150 },
  { date: 150, onset: 999 }, // Weird point, should be ignored
  { date: 999, onset: 140 }, // Weird point, should be ignored
  { date: 249, onset: 199 },
  { date: 250, onset: 200, repeat: 'a' },
  { date: 299, onset: 249, repeat: 'a' },
  { date: 300, onset: 200, repeat: 'b' },
  // End of 'b' is not clear
  { date: 350, onset: 200, repeat: 'c' },
  { date: 399, onset: 249, repeat: 'c' },
  { date: 400, onset: 200, repeat: 'd' },
  { date: 449, onset: 249, repeat: 'd' },
];

const SynchroRepeatsTests = [
  { date: 42, onset: 42, repeat: undefined },
  { date: 148, onset: 148, repeat: 'a' },
  { date: 175, onset: 115, repeat: 'b' },
  { date: 238, onset: 188, repeat: undefined },
  { date: 250, onset: 200, repeat: 'a' },
  // { date: 330, onset: 230, repeat: 'b' }, // Ne passera pas, à discuter
  { date: 399, onset: 249, repeat: 'c' },
  { date: 421, onset: 221, repeat: 'd' },
];

const imageSync: ImageSyncFormat = require('./bwv847/sources/images/bwv847/positions.json');
const audioSync: AudioSyncFormat = require('./bwv847/sources/audios/bwv847/synchro.json');
const audioImageSync: AudioImageSyncFormat = require('./bwv847/sources/audios/bwv847/images/bwv847-waves/positions.json');
const image3DSync: Image3DSyncFormat = require('./schubert-winterreise/sources/images/scan/positions.json');

function round(n: number): number {
  return Math.round(n * 2 ** 15) / 2 ** 15;
}

describe('Unit', () => {
  it('image sync file should parse with no exceptions', async () => {
    UnitFormat.createImageUnit(imageSync);
  });
  it('audio sync file should parse with no exceptions', async () => {
    UnitFormat.createAudioUnit(audioSync);
  });
  it('audio image sync file should parse with no exceptions', async () => {
    const unitSecond = UnitFormat.createAudioUnit(audioSync);
    UnitFormat.createAudioImageUnit(unitSecond, audioImageSync);
  });
  it('image3D sync file should parse with no exceptions', async () => {
    UnitFormat.createImage3DUnit(image3DSync);
  });

  it('audio image unit should be reversible (toOnset is the inverse of fromOnset)', async () => {
    const unitSecond = UnitFormat.createAudioUnit(audioSync);
    const unit = UnitFormat.createAudioImageUnit(unitSecond, audioImageSync);

    for (let i = 0; i <= 100; i += 1) {
      const onset = i as Onset;
      const pixel = i as Pixel;
      expect(round(unit.fromOnset(unit.toOnset(pixel).value).value)).toEqual(pixel);
      expect(round(unit.toOnset(unit.fromOnset(onset).value).value)).toEqual(onset);
    }
  });

  it('image unit should be reversible (toOnset is the inverse of fromOnset)', async () => {
    const unit = UnitFormat.createImageUnit(imageSync);

    for (let i = 0; i <= 120; i += 1) {
      const onset = i as Onset;
      const pixel = i * 72 + 300 as Pixel;
      expect(round(unit.fromOnset(unit.toOnset(pixel).value).value)).toEqual(pixel);
      expect(round(unit.toOnset(unit.fromOnset(onset).value).value)).toEqual(onset);
    }
  });

  it('check that toOnset and fromOnset are not affected by unit optimization: audio', async () => {
    const unitSecond = UnitFormat.createAudioUnit(audioSync);
    const unit = UnitFormat.createAudioImageUnit(unitSecond, audioImageSync);

    const unitFast = unit.getOptimized();

    for (let i = 0; i <= 100; i += 1) {
      const onset = i as Onset;
      const pixel = i as Pixel;
      expect(round(unitFast.toOnset(pixel).value))
        .toEqual(round(unit.toOnset(pixel).value));
      expect(round(unitFast.fromOnset(onset).value))
        .toEqual(round(unit.fromOnset(onset).value));
    }
  });

  it('check that toOnset and fromOnset are not affected by unit optimization: image', async () => {
    const unit = UnitFormat.createImageUnit(imageSync);

    const unitFast = unit.getOptimized();

    for (let i = 0; i <= 120; i += 1) {
      const onset = i as Onset;
      const pixel = i * 72 + 300 as Pixel;

      expect(round(unitFast.toOnset(pixel).value))
        .toEqual(round(unit.toOnset(pixel).value));
      expect(round(unitFast.fromOnset(onset).value))
        .toEqual(round(unit.fromOnset(onset).value));
    }
  });

  /**
   * Here are described the behavior of the onset-pixel conversion object
   * Previous version: https://gitlab.com/algomus.fr/dezrann/-/blob/d5e4a6058bc88440256b513d4bfe9d32fdbc43a6/code/essais/sc-client/test/test.js
   * Issue: https://gitlab.com/algomus.fr/dezrann/-/issues/342
   */

  // A fake pixel-onset conversion format
  const normalPos = [
    { x: 10.0, onset: 0.0 },
    { x: 20.0, onset: 1.0 },
    { x: 30.0, onset: 2.0 },
    { x: 40.0, onset: 3.0 },
    { x: 50.0, onset: 4.0 },
    { x: 60.0, onset: 5.0 },
    { x: 70.0, onset: 6.0 },
    { x: 80.0, onset: 7.0 },
    { x: 85.0, onset: 7.0 }, // two keys can have the same onset
    { x: 90.0, onset: 8.0 },
    { x: 90.0, onset: 8.5 }, // two keys can have the same x positions
    { x: 100.0, onset: 9.0 }, // many keys can be identical
    { x: 100.0, onset: 9.0 },
    { x: 100.0, onset: 9.0 },
    { x: 100.0, onset: 9.0 },
    { x: 100.0, onset: 9.0 },
    { x: 100.0, onset: 9.0 },
    { x: 100.0, onset: 9.0 },
  ];
  const syncFile = {
    'onset-x': normalPos,
    bars: [],
    staffs: [],
  };
  const pixelUnit = UnitFormat.createImageUnit(syncFile);
  const pixel3DUnit = UnitFormat.createImage3DUnit(image3DSync);

  const audioUnitRepeats = UnitFormat.createAudioUnit(SynchroRepeats);
  describe('Unit<Second>.toOnset', () => {
    it('should return repeat "a", onset 100 at date 100', () => {
      const onset = audioUnitRepeats.toOnset(100 as Second);
      expect(onset.value).toEqual(100);
      expect(onset.repeat).toEqual('a');
    });
    it('should return repeat "b", onset 100 at date 150', () => {
      const onset = audioUnitRepeats.toOnset(150 as Second);
      expect(onset.value).toEqual(100);
      expect(onset.repeat).toEqual('b');
    });
    it('should return repeat "a", onset 200 at date 250', () => {
      const onset = audioUnitRepeats.toOnset(250 as Second);
      expect(onset.value).toEqual(200);
      expect(onset.repeat).toEqual('a');
    });
    it('should return repeat "b", onset 200 at date 300', () => {
      const onset = audioUnitRepeats.toOnset(300 as Second);
      expect(onset.value).toEqual(200);
      expect(onset.repeat).toEqual('b');
    });
    it('should return repeat "c", onset 200 at date 350', () => {
      const onset = audioUnitRepeats.toOnset(350 as Second);
      expect(onset.value).toEqual(200);
      expect(onset.repeat).toEqual('c');
    });
    it('should return repeat "d", onset 200 at date 400', () => {
      const onset = audioUnitRepeats.toOnset(400 as Second);
      expect(onset.value).toEqual(200);
      expect(onset.repeat).toEqual('d');
    });
    it('should pass SynchroRepeatsTests tests', () => {
      for (let i = 0; i < SynchroRepeatsTests.length; i += 1) {
        const onset = audioUnitRepeats.toOnset(SynchroRepeatsTests[i].date as Second);
        expect(onset.value).toEqual(SynchroRepeatsTests[i].onset);
        expect(onset.repeat).toEqual(SynchroRepeatsTests[i].repeat);
      }
    });
  });

  describe('Unit<Second>.fromOnset', () => {
    it('should return repeat "a", second 100 at onset 100, repeat undefined', () => {
      const second = audioUnitRepeats.fromOnset(100 as Onset);
      expect(second.value).toEqual(100);
      expect(second.repeat).toEqual('a');
    });
    it('should return repeat "b", second 150 at onset 100, repeat b', () => {
      const second = audioUnitRepeats.fromOnset(100 as Onset, 'b');
      expect(second.value).toEqual(150);
      expect(second.repeat).toEqual('b');
    });
    it('should return repeat "a", second 250 at onset 200, repeat undefined', () => {
      const second = audioUnitRepeats.fromOnset(200 as Onset);
      expect(second.value).toEqual(250);
      expect(second.repeat).toEqual('a');
    });
    it('should return repeat "b", second 300 at onset 200, repeat b', () => {
      const second = audioUnitRepeats.fromOnset(200 as Onset, 'b');
      expect(second.value).toEqual(300);
      expect(second.repeat).toEqual('b');
    });
    it('should return repeat "c", second 350 at onset 200, repeat c', () => {
      const second = audioUnitRepeats.fromOnset(200 as Onset, 'c');
      expect(second.value).toEqual(350);
      expect(second.repeat).toEqual('c');
    });
    it('should return repeat "d", second 400 at onset 200, repeat d', () => {
      const second = audioUnitRepeats.fromOnset(200 as Onset, 'd');
      expect(second.value).toEqual(400);
      expect(second.repeat).toEqual('d');
    });
    it('should pass SynchroRepeatsTests tests', () => {
      for (let i = 0; i < SynchroRepeatsTests.length; i += 1) {
        const second = audioUnitRepeats.fromOnset(
          SynchroRepeatsTests[i].onset as Onset,
          SynchroRepeatsTests[i].repeat,
        );
        expect(second.value).toEqual(SynchroRepeatsTests[i].date);
        expect(second.repeat).toEqual(SynchroRepeatsTests[i].repeat);
      }
    });
  });

  describe('Unit<Pixel>.toOnset', () => {
    it('should work for an exact position', () => {
      expect(pixelUnit.toOnset(30.0 as Pixel).value).toEqual(2);
    });
    it('should snap to an upper position closer than the next note', () => {
      expect(pixelUnit.toOnset(34.0 as Pixel, true).value).toEqual(2);
    });
    it('should snap to a lower position closer than the previous note', () => {
      expect(pixelUnit.toOnset(26.0 as Pixel, true).value).toEqual(2);
    });
    it('should snap to the previous note onset when position is the exact middle between 2 notes', () => {
      expect(pixelUnit.toOnset(35.0 as Pixel, true).value).toEqual(2);
    });
    it('should work with first note', () => {
      expect(pixelUnit.toOnset(10.0 as Pixel).value).toEqual(0);
    });
    it('should work with last note', () => {
      expect(pixelUnit.toOnset(100.0 as Pixel).value).toEqual(9);
    });
    it('should work before first note', () => {
      expect(pixelUnit.toOnset(5.0 as Pixel).value).toEqual(0);
    });
    it('should work after last note', () => {
      expect(pixelUnit.toOnset(105.0 as Pixel).value).toEqual(9);
    });
    it('should work with duplicate onset', () => {
      expect(pixelUnit.toOnset(85.0 as Pixel).value).toEqual(7);
    });
    it('should work with duplicate pixel', () => {
      expect(pixelUnit.toOnset(90.0 as Pixel).value).toEqual(8);
    });
    it('should work with duplicate sync points', () => {
      expect(pixelUnit.toOnset(100 as Pixel).value).toEqual(9);
    });
  });
  describe('Unit<Pixel3D>.toOnset', () => {
    it('should work for an exact position', () => {
      expect(pixel3DUnit.toOnset(199.0 as Pixel).value).toEqual(2);
    });
    it('should snap to an upper position closer than the next note', () => {
      expect(pixel3DUnit.toOnset(195.0 as Pixel, true).value).toEqual(2);
    });
    it('should snap to a lower position closer than the previous note', () => {
      expect(pixel3DUnit.toOnset(205.0 as Pixel, true).value).toEqual(2);
    });
    it('should snap to the previous note onset when position is the exact middle between 2 notes', () => {
      expect(pixel3DUnit.toOnset(244.5 as Pixel, true).value).toEqual(2);
    });
    it('should work with first note', () => {
      expect(pixel3DUnit.toOnset(81.0 as Pixel).value).toEqual(0);
    });
    it('should work with last note', () => {
      expect(pixel3DUnit.toOnset(50594337.0 as Pixel).value).toEqual(208);
    });
    it('should work before first note', () => {
      expect(pixel3DUnit.toOnset(5.0 as Pixel).value).toEqual(0);
    });
    it('should work after last note', () => {
      expect(pixel3DUnit.toOnset(60594337.0 as Pixel).value).toEqual(208);
    });
    it('should work with duplicate onset', () => {
      expect(pixel3DUnit.toOnset(589.0 as Pixel).value).toEqual(12);
    });
    it('should work with duplicate pixel', () => {
      expect(pixel3DUnit.toOnset(523.0 as Pixel).value).toEqual(10);
    });
    it('should work with duplicate sync points', () => {
      expect(pixel3DUnit.toOnset(550 as Pixel).value).toEqual(10);
    });
  });
  describe('Unit<Pixel>.toInterval', () => {
    it('should work with exact notes position : [n1..n2]', () => {
      expect(pixelUnit.toInterval(40 as Pixel, 40 as Pixel).duration).toEqual(4);
    });
    it('should work with snap and approx notes positions : [..n1..]..n2', () => {
      expect(pixelUnit.toInterval(36 as Pixel, 40 as Pixel, true).duration).toEqual(4);
    });
    it('should work with snap and approx notes positions : [..n1..n2..]', () => {
      expect(pixelUnit.toInterval(36 as Pixel, 45 as Pixel, true).duration).toEqual(4);
    });
    it('should work with snap and approx notes positions : n1..[..n2..]', () => {
      expect(pixelUnit.toInterval(44 as Pixel, 40 as Pixel, true).duration).toEqual(4);
    });
    it('should work with snap and approx notes positions : n1..[..]..n2', () => {
      expect(pixelUnit.toInterval(44 as Pixel, 32 as Pixel, true).duration).toEqual(4);
    });
    it('should return zero for length of zero', () => {
      expect(pixelUnit.toInterval(40 as Pixel, 0 as Pixel).duration).toEqual(0);
    });
    it('should return the right length for an out of bound graphical length', () => {
      expect(pixelUnit.toInterval(40 as Pixel, 300 as Pixel).duration).toEqual(6);
    });
    it('should return the piece length for an out of bound x', () => {
      expect(pixelUnit.toInterval(0 as Pixel, 50 as Pixel).duration).toEqual(4);
    });
    it('should return the piece length for an out of bound x and graphical length', () => {
      expect(pixelUnit.toInterval(0 as Pixel, 300 as Pixel).duration).toEqual(9);
    });
  });

  describe('Unit<Pixel3D>.toInterval', () => {
    it('should work with exact notes position : [n1..n2]', () => {
      expect(pixel3DUnit.toInterval(199 as Pixel, 169 as Pixel).duration).toEqual(4);
    });
    it('should work with snap and approx notes positions : [..n1..]..n2', () => {
      expect(pixel3DUnit.toInterval(193 as Pixel, 169 as Pixel, true).duration).toEqual(4);
    });
    it('should work with snap and approx notes positions : [..n1..n2..]', () => {
      expect(pixel3DUnit.toInterval(193 as Pixel, 175 as Pixel, true).duration).toEqual(4);
    });
    it('should work with snap and approx notes positions : n1..[..n2..]', () => {
      expect(pixel3DUnit.toInterval(204 as Pixel, 169 as Pixel, true).duration).toEqual(4);
    });
    it('should work with snap and approx notes positions : n1..[..]..n2', () => {
      expect(pixel3DUnit.toInterval(204 as Pixel, 160 as Pixel, true).duration).toEqual(4);
    });
    it('should return zero for length of zero', () => {
      expect(pixel3DUnit.toInterval(199 as Pixel, 0 as Pixel).duration).toEqual(0);
    });
    it('should return the right length for an out of bound graphical length', () => {
      expect(pixel3DUnit.toInterval(50594269 as Pixel, 300 as Pixel).duration).toEqual(2);
    });
    it('should return the piece length for an out of bound x', () => {
      expect(pixel3DUnit.toInterval(0 as Pixel, 290 as Pixel).duration).toEqual(4);
    });
    it('should return the piece length for an out of bound x and graphical length', () => {
      expect(pixel3DUnit.toInterval(0 as Pixel, 60594337 as Pixel).duration).toEqual(208);
    });
  });

  describe('Unit<Pixel>.snapOnset', () => {
    it('should snap to an upper onset closer than the next note', () => {
      expect(pixelUnit.snapOnset(2.25 as Onset)).toEqual(2);
    });
    it('should snap to a lower onset closer than the previous note', () => {
      expect(pixelUnit.snapOnset(1.75 as Onset)).toEqual(2);
    });
    it('should snap to the previous note position when onset is the exact middle between 2 notes', () => {
      expect(pixelUnit.snapOnset(2.5 as Onset)).toEqual(2);
    });
  });

  describe('Unit<Pixel3D>.snapOnset', () => {
    it('should snap to an upper onset closer than the next note', () => {
      expect(pixel3DUnit.snapOnset(2.25 as Onset)).toEqual(2);
    });
    it('should snap to a lower onset closer than the previous note', () => {
      expect(pixel3DUnit.snapOnset(1.75 as Onset)).toEqual(2);
    });
    it('should snap to the previous note position when onset is the exact middle between 2 notes', () => {
      expect(pixel3DUnit.snapOnset(2.5 as Onset)).toEqual(2);
    });
  });

  describe('Unit<Pixel>.snapInterval', () => {
    it('should work with approx non existing onset : [..n1..n2..]', () => {
      expect(pixelUnit.snapInterval(2.75 as Onset, 4.5 as Duration).duration).toEqual(4);
    });
    it('should work with approx non existing onset : n1..[..n2..]', () => {
      expect(pixelUnit.snapInterval(3.25 as Onset, 4 as Duration).duration).toEqual(4);
    });
    it('should work with approx non existing onset : n1..[..]..n2', () => {
      expect(pixelUnit.snapInterval(3.25 as Onset, 3.5 as Duration).duration).toEqual(4);
    });
  });

  describe('Unit<Pixel3D>.snapInterval', () => {
    it('should work with approx non existing onset : [..n1..n2..]', () => {
      expect(pixel3DUnit.snapInterval(2.75 as Onset, 4.5 as Duration).duration).toEqual(6);
    });
    it('should work with approx non existing onset : n1..[..n2..]', () => {
      expect(pixel3DUnit.snapInterval(3.25 as Onset, 4 as Duration).duration).toEqual(4);
    });
    it('should work with approx non existing onset : n1..[..]..n2', () => {
      expect(pixel3DUnit.snapInterval(3.25 as Onset, 3.5 as Duration).duration).toEqual(2);
    });
  });

  describe('Unit<Pixel>.fromOnset', () => {
    it('should work for an existing onset', () => {
      expect(pixelUnit.fromOnset(2 as Onset).value).toEqual(30);
    });
    it('should work with first note', () => {
      expect(pixelUnit.fromOnset(0 as Onset).value).toEqual(10);
    });
    it('should work with last note', () => {
      expect(pixelUnit.fromOnset(9 as Onset).value).toEqual(100);
    });
    it('should work after last note', () => {
      expect(pixelUnit.fromOnset(10 as Onset).value).toEqual(100);
    });
    it('should work with duplicate onset', () => {
      expect(pixelUnit.fromOnset(7 as Onset).value).toEqual(80.0);
    });
    it('should work with duplicate pixel', () => {
      expect(pixelUnit.fromOnset(8 as Onset).value).toEqual(90.0);
    });
    it('should work with duplicate sync points', () => {
      expect(pixelUnit.fromOnset(9 as Onset).value).toEqual(100);
    });
  });

  describe('Unit<Pixel3D>.fromOnset', () => {
    it('should work for an existing onset', () => {
      expect(pixel3DUnit.fromOnset(2 as Onset).value).toEqual(199);
    });
    it('should work with first note', () => {
      expect(pixel3DUnit.fromOnset(0 as Onset).value).toEqual(81);
    });
    it('should work with last note', () => {
      expect(pixel3DUnit.fromOnset(208 as Onset).value).toEqual(50594337.0);
    });
    it('should work after last note', () => {
      expect(pixel3DUnit.fromOnset(300 as Onset).value).toEqual(50594337.0);
    });
    it('should work with duplicate onset', () => {
      expect(pixel3DUnit.fromOnset(12 as Onset).value).toEqual(589.0);
    });
    it('should work with duplicate pixel', () => {
      expect(pixel3DUnit.fromOnset(10 as Onset).value).toEqual(523.0);
    });
    it('should work with duplicate sync points', () => {
      expect(pixel3DUnit.fromOnset(10 as Onset).value).toEqual(523);
    });
  });

  describe('Unit<Pixel>.fromInterval', () => {
    it('should work with exact notes onset : [n1..n2]', () => {
      expect(pixelUnit.fromInterval(3 as Onset, 4 as Duration).width).toEqual(40);
    });
    it('should work with non existing onset : [..n1..]..n2', () => {
      expect(pixelUnit.fromInterval(2.75 as Onset, 4 as Duration).width).toEqual(40);
    });
    it('should return zero for duration of zero', () => {
      expect(pixelUnit.fromInterval(3 as Onset, 0 as Duration).width).toEqual(0);
    });
    it('should return the right length for an out of bound duration', () => {
      expect(pixelUnit.fromInterval(3 as Onset, 30 as Duration).width).toEqual(60);
    });
    it('should return the right length for an out of bound start onset', () => {
      expect(pixelUnit.fromInterval(0 as Onset, 5 as Duration).width).toEqual(50);
    });
    it('should return the piece length for an out of bound start onset and duration', () => {
      expect(pixelUnit.fromInterval(0 as Onset, 30 as Duration).width).toEqual(90);
    });
  });

  describe('Unit<Pixel3D>.fromInterval', () => {
    it('should work with exact notes onset : [n1..n2]', () => {
      expect(pixel3DUnit.fromInterval(2 as Onset, 2 as Duration).width).toEqual(91);
    });
    it('should work with non existing onset : [..n1..]..n2', () => {
      expect(pixel3DUnit.fromInterval(2.75 as Onset, 2 as Duration).width).toEqual(86.125);
    });
    it('should return zero for duration of zero', () => {
      expect(pixel3DUnit.fromInterval(3 as Onset, 0 as Duration).width).toEqual(0);
    });
    it('should return the right length for an out of bound duration', () => {
      expect(pixel3DUnit.fromInterval(206 as Onset, 30 as Duration).width).toEqual(68);
    });
    it('should return the right length for an out of bound start onset', () => {
      expect(pixel3DUnit.fromInterval(0 as Onset, 4 as Duration).width).toEqual(209);
    });
    it('should return the piece length for an out of bound start onset and duration', () => {
      expect(pixel3DUnit.fromInterval(0 as Onset, 300 as Duration).width).toEqual(50594256);
    });
  });
});
