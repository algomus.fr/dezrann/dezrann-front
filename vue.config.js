module.exports = {
  lintOnSave: true,

  transpileDependencies: [
    'vuetify',
  ],

  configureWebpack: {
    devtool: 'source-map',

    watchOptions: {
      poll: true,
    },

    // https://github.com/webpack/webpack/issues/14532
    output: {
      hashFunction: 'xxhash64',
    },
  },

  // chainWebpack: (config) => {
  //   config.module
  //     .rule('i18n')
  //     .resourceQuery(/blockType=i18n/)
  //     .type('javascript/auto')
  //     .use('i18n')
  //     .loader('@intlify/vue-i18n-loader')
  //     .end()
  //     .use('yaml')
  //     .loader('yaml-loader')
  //     .end();
  // },

  publicPath: process.env.DEV_BUILD_PATH ? `/dev/${process.env.DEV_BUILD_PATH}/` : '/',

  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      enableInSFC: false,
      localeDir: 'locales',
      enableBridge: false,
    },
  },
  devServer: {
    client: {
      overlay: {
        errors: true,
        warnings: false,
        runtimeErrors: false,
      },
    },
  },
};
